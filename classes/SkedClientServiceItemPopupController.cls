public class SkedClientServiceItemPopupController {
    public String query {get; set;}
	public List<enrtcr__Support_Contract_Item__c> accounts {get; set;}
    
    public List<enrtcr__Support_Contract_Item__c  > getServiceAgreementItems() {
        
        List<enrtcr__Support_Contract_Item__c  > results = Database.query(
            'SELECT Id, Name, enrtcr__Site__c, enrtcr__Service__c, enrtcr__Rate__c, enrtcr__Quantity__c, enrtcr__Total__c, enrtcr__Lead_Provider__c, enrtcr__Delivery_Location__c, enrtcr__Delivered__c  ' +
            'FROM enrtcr__Support_Contract_Item__c  ' +
            'LIMIT 10'
        );
        return results;
    }
    
    public PageReference runQuery()
	{
		List<List<enrtcr__Support_Contract_Item__c>> searchResults=[FIND :query IN ALL FIELDS RETURNING enrtcr__Support_Contract_Item__c (Id, Name, enrtcr__Site__c, enrtcr__Service__c, enrtcr__Rate__c, enrtcr__Quantity__c, enrtcr__Total__c, enrtcr__Lead_Provider__c, enrtcr__Delivery_Location__c, enrtcr__Delivered__c)];
		accounts=searchResults[0];
  		return null;
	}

}