public class SkedJobDetailController { 

    public sked__Job__c job{get;set;}
    public Job_Service_Item__c newServiceItem{get;set;}
    private String jobId;
    public String mode{get;set;}

    public Id defaultResourceId{
        get{
            if(defaultResourceId==null){
                list<sked__Resource__c> resources = [Select Id from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];
                if(!resources.isEmpty())  defaultResourceId = resources.get(0).Id;
            }
            return defaultResourceId;
        }
        set{}
    }
    
    public SkedJobDetailController(){
        mode = 'items';
        newServiceItem = new Job_Service_Item__c(Worker__c=defaultResourceId);
        jobId = ApexPages.currentPage().getParameters().get('jobId');
        queryJob();
    }

    public void queryJob(){
        if( !String.isBlank(jobId) ){
            job = [Select Id, Name, sked__Account__r.Name, Service_Agreement__c, sked__Contact__r.Name, sked__Address__c, sked__Start__c, sked__Finish__c, Service_Options__c,
                    (Select Id, Name, Comment__c, Quantity__c, Travel_Kms__c, UOM__c, Service_Agreement_Item__c, Service_Agreement_Item__r.enrtcr__Support_Contract__r.Name, Service_Agreement_Item__r.enrtcr__Service__r.Name, Service_Agreement_Item__r.enrtcr__Service_Type__c   from Job_Service_Item__r)
                    from sked__Job__c where Id = :jobId];
        }
    }

    private string getServiceName(enrtcr__Support_Contract_Item__c item)
    {
        Decimal remainingBalance = item.enrtcr__Total__c - (item.enrtcr__Delivered__c==null?0:item.enrtcr__Delivered__c);
        String sitePrefix = (String.isEmpty(item.enrtcr__Site__r.enrtcr__Site_Code__c)) ? (String.isEmpty(item.enrtcr__Site__r.Name)?null: item.enrtcr__Site__r.Name) : item.enrtcr__Site__r.enrtcr__Site_Code__c;
        sitePrefix = sitePrefix!=null?sitePrefix + ' - ':'';

        String serviceName =  sitePrefix + item.enrtcr__Service__r.Name + (item.enrtcr__Service_Type__c ==null?'': (' - ' + item.enrtcr__Service_Type__c ));
        if(!item.enrtcr__Support_Contract__r.enrtcr__Mac_Funded__c)
        {
            serviceName += ' - ($'+ String.format(remainingBalance.format(), new String[]{'0','number','###,###,##0.00'})  +')';
        }

        return serviceName;
    }

    public list<SelectOption> getServiceItems(){
        list<SelectOption> result = new list<SelectOption>();
        if(job.Service_Agreement__c==null) return result;
        
        Set<Id> addedServiceItems = new Set<Id>();
        for( Job_Service_Item__c jsi : job.Job_Service_Item__r ){
            addedServiceItems.add( jsi.Service_Agreement_Item__c );
        }
        
		list<enrtcr__Support_Contract_Item__c> serviceItems = [select id,enrtcr__Support_Contract__r.enrtcr__Mac_Funded__c,enrtcr__Rate__r.enrtcr__RateType__r.Name,enrtcr__Rate__r.Name,enrtcr__Total__c,enrtcr__Delivered__c,enrtcr__Site__r.Name,enrtcr__Site__r.enrtcr__Site_Code__c, enrtcr__Service__r.Name, enrtcr__Service_Type__c  from enrtcr__Support_Contract_Item__c where enrtcr__Support_Contract__c = :job.Service_Agreement__c];
        
        for(enrtcr__Support_Contract_Item__c si : serviceItems){
            if(!addedServiceItems.contains(si.Id)){
                result.add( new SelectOption( si.Id, getServiceName(si )) );
            }
        }

        return result;
    }

    public void viewServiceItems(){
        mode = 'items';
    }

    public void addServiceItems(){
        mode = 'add';
    }

    public void cancel(){
        mode = 'items';
    }

    public void back(){
        mode = 'job';
    }

    public void saveServiceItem(){
        newServiceItem.Job__c = job.Id;
        //Get UOM
        list<ServiceItem> siList = (list<ServiceItem>)JSON.deserialize(job.Service_Options__c, list<ServiceItem>.class);
        for(ServiceItem si : siList){
            if(newServiceItem.Service_Agreement_Item__c == si.ServiceId){
                newServiceItem.UOM__c = si.UnitOfMeasure;
                break;
            }
        }
        insert newServiceItem;
        newServiceItem = new Job_Service_Item__c(Worker__c=defaultResourceId);
        queryJob();
        mode = 'items';
    }

    public class ServiceItem{
        public String ServiceId{get;set;}
        public String Name{get;set;}
        public Decimal Rate{get;set;}
        public String UnitOfMeasure{get;set;}
    }
}