public class skedAddressOption {
	public string id {get;set;}
    public string label {get;set;}
    public string value {get;set;}
    public string type {get;set;}
    public string address {get;set;}
    public string locationID {get;set;}
    public string addressID {get;set;}
    public string regionID {get;set;}
    public string pcode {get;set;}
    public string rickassessmnet {get;set;}
    public double lng {get;set;}
    public double lat {get;set;}
}