public class skedAppointmentDetail { 
	public string jobId {get;set;}
	public string JobNo {get;set;}
	public string startDateTime {get;set;}
	public string duration {get;set;}
	public string noAttendees {get;set;}
	public string status {get;set;}

	public skedAppointmentDetail(){}
	public skedAppointmentDetail(sked__Job__c job){
		this.jobId = Job.id;
		this.JobNo = Job.Name;
		this.startDateTime = job.sked__Start__c ==null?'':(job.sked__Start__c.format('dd/MM/yyyy h:mma',job.sked__Region__r.sked__Timezone__c));
		this.duration = string.valueOf(job.sked__Duration__c);
		this.status = job.sked__Job_Status__c;
		this.noAttendees = string.valueOf(job.No_of_Attendees__c);
	}
}