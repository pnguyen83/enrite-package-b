global class skedAttendeeAvailableServiceBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.StateFul {
  
  String query;
  list<id> attIds = new list<id>();
  string sessId;
  
  //get list Attendee ID and current user sessionId
  global skedAttendeeAvailableServiceBatch(list<id> lstAttendees,string sessionId) {
    attIds = lstAttendees;
    sessId = sessionId;
  }
  
  //Query group Attendee
  global Database.QueryLocator start(Database.BatchableContext BC) {
    query = 'select id from Group_Attendee__c where id in:attIds';
    return Database.getQueryLocator(query);
  }

    global void execute(Database.BatchableContext BC, List<Group_Attendee__c> scope) {

      //get Map Attendee and available service json
      list<Attachment> lstAttachments = new list<Attachment>();
      map<id,Attachment> mapIDAttachment = new Map<id,Attachment>();

      for(Attachment att: [Select Id, Name,ParentId,Body From Attachment where ParentId in: scope]){
        mapIDAttachment.put(att.ParentId,att);
      }

      //get Group Attendee and it Job format date yyyy-MM-dd to it Timezone
      for(Group_Attendee__c GA: [select id,Job_Start__c,Contact__c,Job__r.sked__Region__r.sked__Timezone__c from Group_Attendee__c where id in:scope]){
        string Servicebody = skedJsonAvailableServiceUtil.getServiceItems(GA.Contact__c,GA.Job_Start__c.format('yyyy-MM-dd',GA.Job__r.sked__Region__r.sked__Timezone__c),sessId);
        Attachment att = new Attachment();
        if(mapIDAttachment.containskey(GA.id)){
          att = mapIDAttachment.get(GA.id);
        }else{
          att.ParentId = GA.id;
        }
        
        //Convert String to BLod
        att.Body= Blob.valueOf(Servicebody);
        att.name = 'available_service.json';
      lstAttachments.add(att);
      }
      //Upsert file
      upsert lstAttachments;
  }
  
  global void finish(Database.BatchableContext BC) {
    
  }
  
}