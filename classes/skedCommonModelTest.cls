@isTest(SeeAllData=true)
private class skedCommonModelTest
{
	static testmethod void doTest() {
		

		Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));

		Map<String, sObject> mapData = skedTestUtilities.createData();
		sked__Region__c region = (sked__region__c)mapData.get('region 1');
		sked__Location__c location = (sked__Location__c)mapData.get('location 1');
		sked__job__c job = (sked__job__c)mapData.get('job 1');
		Contact cont = (Contact)mapData.get('contact 1');
		skedCommonModels.SlotModel slotMOdel = new skedCommonModels.SlotModel(job, job.sked__Start__c.addMinutes(-1), job.sked__Finish__c.addMinutes(1));
		String jsonStr = JSON.serialize(slotModel);

		test.starttest();
		
		skedCommonModels.getRegions(region.name);
		skedCommonModels.getLocations(location.Name, location.sked__region__c);
		skedCommonModels.searchSites('test');
		skedCommonModels.getAbortReasons();
		skedCommonModels.Item item = new skedCommonModels.Item(job.Id, 'test label', 20);

		skedCommonModels.getNextRoster();

		job.sked__Notes_Comments__c = 'test';
		update job;

		//delete alert;
		test.stoptest();
	}

	static testmethod void testAlertTrigger() {
		test.starttest();

		Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));

		Map<String, sObject> mapData = skedTestUtilities.createData();
		sked__Region__c region = (sked__region__c)mapData.get('region 1');
		sked__Location__c location = (sked__Location__c)mapData.get('location 1');
		sked__job__c job = (sked__job__c)mapData.get('job 1');
		Contact cont = (Contact)mapData.get('contact 1');
		skedCommonModels.SlotModel slotMOdel = new skedCommonModels.SlotModel(job, job.sked__Start__c.addMinutes(-1), job.sked__Finish__c.addMinutes(1));
		String jsonStr = JSON.serialize(slotModel);

		enrtcr__Alert__c alert = new enrtcr__Alert__c(enrtcr__Client__c=cont.Id);
		insert alert;

		

		//delete alert;
		test.stoptest();
	}
}