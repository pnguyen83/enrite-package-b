public class skedConfigs {
	
	public static String LOCATION_OR_SITE_FILTER{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_LocationOrSiteFilter')){
                LOCATION_OR_SITE_FILTER     = skedConfigs__c.getAll().get('RM_LocationOrSiteFilter').Value__c;
            }else LOCATION_OR_SITE_FILTER   = 'Location';
            return LOCATION_OR_SITE_FILTER;
        }
        set;
    }

    public static String JOB_URL{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_JobURL')){
                JOB_URL     = skedConfigs__c.getAll().get('Global_JobURL').Value__c;
            }else JOB_URL   = '/';
            return JOB_URL;
        }
        set;
    }

    public static Integer SCHEDULE_START{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_Start')){
                SCHEDULE_START     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_Start').Value__c);
            }else SCHEDULE_START   = 0;
            return SCHEDULE_START;
        }
        set;
    }

    public static Integer SCHEDULE_END{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_End')){
                SCHEDULE_END     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_End').Value__c);
            }else SCHEDULE_END   = 2300;
            return SCHEDULE_END;
        }
        set;
    }

    public static Integer SCHEDULE_INTERVAL{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_Interval')){
                SCHEDULE_INTERVAL     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_Interval').Value__c);
            }else SCHEDULE_INTERVAL   = 60;
            return SCHEDULE_INTERVAL;
        }
        set;
    }

    public static String SKEDULO_API_TOKEN{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_SkeduloAPIToken')){
                SKEDULO_API_TOKEN     = skedConfigs__c.getAll().get('Global_SkeduloAPIToken').Value__c;
            }else SKEDULO_API_TOKEN   = '';
            return SKEDULO_API_TOKEN;
        }
        set;
    }

    public static String GOOGLE_API_KEY{
        get{
            if(GOOGLE_API_KEY == null){
                if(skedConfigs__c.getAll().containsKey('Global_GoogleAPIKEY')){
                    GOOGLE_API_KEY     = skedConfigs__c.getAll().get('Global_GoogleAPIKEY').Value__c;
                }else GOOGLE_API_KEY   = '';
            }
            return GOOGLE_API_KEY;
        }
    }

    public static String GEOCODING_SERVICE{
        get{
            if(GEOCODING_SERVICE == null){
                if(skedConfigs__c.getAll().containsKey('Geocoding_Service')){
                    GEOCODING_SERVICE     = skedConfigs__c.getAll().get('Geocoding_Service').Value__c;
                }else GEOCODING_SERVICE   = 'Skedulo';
            }
            return GEOCODING_SERVICE;
        }
    }

    public static String RESOURCE_DISTANCE{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_ResourceDistance')){
                RESOURCE_DISTANCE     = skedConfigs__c.getAll().get('Global_ResourceDistance').Value__c;
            }else RESOURCE_DISTANCE   = 'First';
            return RESOURCE_DISTANCE;
        }
        set;
    }

    public static String DISTANCE_TRAVELLED_SPLIT{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_DistanceTravelledSplit')){
                DISTANCE_TRAVELLED_SPLIT     = skedConfigs__c.getAll().get('Global_DistanceTravelledSplit').Value__c;
            }else DISTANCE_TRAVELLED_SPLIT   = 'All';
            return DISTANCE_TRAVELLED_SPLIT;
        }
        set;
    }

    public static boolean SHOW_ESTIMATED_DISTANCE_TRAVELLED{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_ShowEstimatedDistanceTravelled')){
                SHOW_ESTIMATED_DISTANCE_TRAVELLED     = Boolean.valueOf(skedConfigs__c.getAll().get('Global_ShowEstimatedDistanceTravelled').Value__c);
            }else SHOW_ESTIMATED_DISTANCE_TRAVELLED   = false;
            return SHOW_ESTIMATED_DISTANCE_TRAVELLED;
        }
        set;
    }

    public static String RESOURCE_DISTANCE_FIRST    = 'First';
    public static String RESOURCE_DISTANCE_COMBINED    = 'Combined';
    public static String DISTANCE_TRAVELLED_SPLIT2    = 'Split';
    public static String DISTANCE_TRAVELLED_ALL    = 'All';

    public static String DEFAULT_DELIVERY_METHOD{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_DefaultDeliveryMethod')){
                DEFAULT_DELIVERY_METHOD     = skedConfigs__c.getAll().get('Global_DefaultDeliveryMethod').Value__c;
            }else DEFAULT_DELIVERY_METHOD   = '';
            return DEFAULT_DELIVERY_METHOD;
        }
        set;
    }
}