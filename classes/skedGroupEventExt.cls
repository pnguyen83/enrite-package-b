/**
* @author admin@skedulo.com.crdev
* @description Group Event 
*/
public class skedGroupEventExt extends skedCommonModels{
    private static final String FIELD_CUSTOM_VALIDATION_EXCEPTION = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, ';
    private final sObject mysObject;

    public String jobURL{
        get{
            return skedConfigs.JOB_URL;
        }
    }


    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public skedGroupEventExt(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
    }

    public String getRecordName() {
        return 'Hello ' + (String)mysObject.get('name') + ' (' + (Id)mysObject.get('Id') + ')';
    }
    
    /*
    * Get Config data
    */
    @RemoteAction
    public static ConfigData getConfigData(){
        return skedCommonModels.getConfigData(skedCommonModels.GROUP_EVENT);
    }

    /**
    * @description get all Region in the System
    * @return list<skedOption> List of Option id, label
    */
    @remoteAction
    public static list<skedOption> getAllRegions(){
        list<sked__Region__c> regs = [select id,Name from sked__Region__c order by Name];
        list<skedOption> lstOptions = new list<skedOption>();

        for(sked__Region__c reg: regs){
            lstOptions.add(new skedOption(reg.id,reg.Name));
        }
        
        return lstOptions;
    }

    /**
    * @description get Group Event
    * @param groupId if blank -> create new, if id - > update
    * @return skedGroupEvent 
    */
    @remoteAction
    public static skedGroupEvent getSkedGroupEvent(string groupId, String locationId){
        if(String.isBlank(groupId)){
            skedGroupEvent ge =  new skedGroupEvent();
            
            if(!String.isBlank(locationId)){
                ge.altAddressLookup = skedConfigs.LOCATION_OR_SITE_FILTER;
                if(ge.altAddressLookup == 'Location'){
                    sked__Location__c location = [Select Id, Name, sked__Address__c from sked__Location__c where Id=:locationId];
                    ge.site  = new skedOption(location.Id, location.Name, location.sked__Address__c);
                }else if(ge.altAddressLookup == 'Site'){
                    enrtcr__Site__c site = [Select Id, Name, Address__c from enrtcr__Site__c where Id=:locationId];
                    ge.site  = new skedOption(site.Id, site.Name, site.Address__c);
                }
                ge.alternativeAddress = ge.site.key;
            }
            return ge;
        }else{
            return new skedGroupEvent(groupId);
        }
    }

    /**
    * @description get Group Event
    * @param groupId if blank -> create new, if id - > update
    * @return skedGroupEvent 
    */
    @remoteAction
    public static skedGroupEvent cancelJob(string groupId,string jobId, string reason){
         
         sked__Job__c job = new sked__job__c (
            id = jobId,
            sked__Job_Status__c = 'Cancelled',
            sked__Abort_Reason__c = reason
         );


         update job;

         return new skedGroupEvent(groupId);
    }

    /**
    * @description Search all resources in system // possible change to get only resource in specific region
    * @param N charactor for search
    * @return list<skedOption> 
    */
    @remoteAction
    public static list<skedOption> searchFacilitator(string N){

        list<skedOption> rtn = new list<skedOption>();
        string lN = '%'+N+'%';
        for(Contact con : [select id, Name,AccountID from Contact where Name like :lN and RecordType.Name <> 'Client' order by Name]){
            rtn.add(new skedOption(con.Id,con.Name,con.AccountID));
        }
        return rtn;
    }


    /**
    * @description Search all Account in system 
    * @param N charactor for search
    * @return list<skedOption> 
    */
    @remoteAction
    public static list<skedOption> searchAccount(string N){
        list<skedOption> rtn = new list<skedOption>();
        string lN = '%'+N+'%';
        for(Account acc : [select id, Name,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry from Account where Name like :lN order by Name]){
            rtn.add(new skedOption(acc.Id,acc.Name,createAddressStringAcc(acc)));
        }
        return rtn;
    }


    public static String createAddressStringAcc(Account acc){
        if(acc == null) return '';
        if (String.isNotBlank(acc.ShippingStreet)) {
            String address = acc.ShippingStreet;
            if(String.isNotBlank(acc.ShippingCity)) address += ', ' + acc.ShippingCity;
            if(String.isNotBlank(acc.ShippingState)) address += ', ' + acc.ShippingState;
            if(String.isNotBlank(acc.ShippingPostalCode)) address += ', ' + acc.ShippingPostalCode;
            if(String.isNotBlank(acc.ShippingCountry)) address += ' ' + acc.ShippingCountry;
            return address;
        }else return '';     
    }


    /**
    * @description 
    * @param N 
    * @return list<skedOption> 
    */
    @remoteAction
    public static list<skedOption> searchServiceItem(string N){
        list<skedOption> rtn = new list<skedOption>();
        string lN = '%'+N+'%';
        for(enrtcr__Service__c SI : [select id, Name, enrtcr__Service_Type__c
        from enrtcr__Service__c where Name like :lN AND SLCS_Group_Event__c = TRUE order by Name]){
            rtn.add(new skedOption(SI.Id,SI.Name + (String.isBlank(SI.enrtcr__Service_Type__c)?'':(' - ' +SI.enrtcr__Service_Type__c))));
        }
        return rtn;
    }

    /**
    * @description 
    * @param N 
    * @return list<skedOption> 
    */
    @remoteAction
    public static list<skedGroupClient> searchAtteedees(string jobId){
        try {
            list<Group_Attendee__c> attendees = [select id, Account__r.Name, Contact__c, Contact__r.Name from Group_Attendee__c where Job__c = :jobId];
            list<skedGroupClient> result = new list<skedGroupClient>();
            if (attendees != null && attendees.size() > 0) {
                for (Group_Attendee__c attendee : attendees) {
                    skedGroupClient GC = new skedGroupClient();
                    GC.id = attendee.Contact__c;
                    GC.groupId = attendee.id;
                    GC.name = attendee.Contact__r.Name;
                    GC.account = attendee.Account__r.Name;
                    result.add(GC);
                }
            }
            system.debug('size ' + result.size());
            system.debug('result ' + result);
            return result;
        } catch (exception ex) {
            system.debug('exception = ' + ex);
        }
        
        return new list<skedGroupClient>();
    }

    //Search Client
    /**
    * @description
    * @param N
    * @return list<skedGroupClient>
    */
    @remoteAction
    public static list<skedGroupClient> searchClients(string N)
    {
        return searchClientsWithActiveServiceAgreements(N,'');
    }
     //Search Client
    /**
    * @description 
    * @param N 
    * @return list<skedGroupClient> 
    */
    @remoteAction
    public static list<skedGroupClient> searchClientsWithActiveServiceAgreements(string N, string clients){
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        list<skedGroupClient> rtn = new list<skedGroupClient>();
        string lN = '%'+N+'%';
        DateTime birthDate;

        Set<Id> clientIds = new Set<Id>();
        for(enrtcr.ListOption l : enrtcr.CustomRemoteActionController.getClientsWithServiceAgreements(null,N,false, new List<string>{'Current','Draft'}))
        {
            if(!clients.contains(l.id))
                clientIds.add(l.id);
        }

        for(Contact con : [select id, Name, Birthdate, MobilePhone, Account.Name,
                           (select id, Blacklist_Client__c from Client_Blacklist__r),
                           (select Id, Client__c, Tag__c, Tag__r.Name, Weighting__c, Required__c FROM Client_Tags__r)
                           from Contact 
                           where id in : clientIds]){
            //rtn.add(new skedGroupClient(acc.Id,acc.Name));
                     
           skedGroupClient GC = new skedGroupClient();
            GC.id = con.id;
            GC.name = con.name;
                               if(con.Birthdate != null) {
                                   birthDate = Datetime.newInstance(con.Birthdate.year(), con.Birthdate.month(), con.Birthdate.day());
                                   GC.birthDate = birthDate.format('dd/MM/yyyy');
                                   if(con.MobilePhone != null) {
                                       GC.label = con.name + ' - MOB ' + con.MobilePhone + ' - DOB ' + birthDate.format('dd/MM/yyyy');
                                   }
                                   else {
                                       GC.label = con.name + ' - DOB ' + birthDate.format('dd/MM/yyyy');
                                   }
                               } 
                               else {
                                   if(con.MobilePhone != null) {
                                       GC.label = con.name + ' - MOB ' + con.MobilePhone ;
                                   }
                                   else {
                                       GC.label = con.name;
                                   }                                   
                               }
            
            GC.contactNumber = con.MobilePhone;
            GC.account = con.Account.Name;

            GC.blacklist = new List<string>();
            Set<String> stBlacklist = new Set<String>();
            for(Client_Blacklist__c CB: con.Client_Blacklist__r){
                stBlacklist.add(CB.Blacklist_Client__c);
                //GC.blacklist.add(CB.Blacklist_Client__c);
            }
            GC.blacklist.addAll(stBlacklist);
            
            GC.tags = new List<skedTag>();
            for ( Client_Tag__c cTag : con.Client_Tags__r ) {
                GC.tags.add(new skedTag(cTag));
            }                 
            rtn.add(GC);
        }
        return rtn;
    }


    /**
    * @description 
    * @param GE 
    * @return string 
    */
    @remoteAction
    public static string createJob(skedGroupEvent GE, List<skedTag> jobTags){
        saveGroupEvent(GE);

        Time startTime = Time.newInstance(Integer.valueOf(GE.startTime.split(':')[0]), Integer.valueOf(GE.startTime.split(':')[1]),0,0);

        sked__Job__c job = new sked__Job__c(
                sked__Job_Status__c = 'Queued',
                sked__Start__c = DateTime.newInstance(Date.parse(GE.startDate), startTime),
                sked__Urgency__c = GE.urgency,
                Group_Event__c = GE.groupId,
                sked__Type__c = GE.jobType,
                sked__Description__c = GE.description,
                sked__Duration__c = GE.duration,
                sked__Region__c =  GE.region.id == 'none'?'':GE.region.id,
                sked__contact__c = string.isBlank(GE.facilitator.id)?null:GE.facilitator.id,
                sked__account__c = string.isBlank(GE.facilitator.key)?null:GE.facilitator.key
        );

        job.sked__Finish__c = job.sked__Start__c.addMinutes(integer.valueOf(job.sked__Duration__c));

        if(string.isBlank(GE.alternativeAddress)){
            job.sked__Address__c = GE.location.address;
        }else{
            job.sked__Address__c = Ge.alternativeAddress;
        }
         if(GE.altAddressLookup == 'Location'){
            job.sked__Location__c    = string.isBlank(GE.site.id)?null:GE.site.id;
            job.Site__c        = null;
        } else if(GE.altAddressLookup == 'Site'){
            job.Site__c = string.isBlank(GE.site.id)?null:GE.site.id;
            job.sked__Location__c    = null;
        }

        insert Job;
        List<sked__Job_Tag__c> insJobTags = new List<sked__Job_Tag__c>();
        for ( skedTag model : jobTags ) {
            sked__Job_Tag__c jobTag = new sked__Job_Tag__c(
                sked__Job__c = job.Id,
                sked__Tag__c = model.id,
                sked__Required__c = model.required,
                sked__Weighting__c = model.required ? null : model.weighting
            );

            insJobTags.add(jobTag);
        }
        if ( insJobTags.size() >0 ) insert insJobTags;
        return job.id;

    }


    /**
    * @description 
    * @param GE 
    * @return string 
    */
    @remoteAction
    public static string saveGroupEvent(skedGroupEvent GE){
        Group_Event__c upGE = [select id,GeoLocation__latitude__s,Account__c,GeoLocation__longitude__s, Name, Address__c,
                                Description__c,Group_Status__c,Region__c,Region__r.Name,Coordinator__c,Coordinator__r.Name,Service__c,
                                Service__r.Name,Service__r.enrtcr__Service_Type__c , 
                                (select id, client__c, client__r.Name, client__r.MobilePhone, service_agreement_item__c, service__c,Service__r.Name, rate__c 
                                from Group_Clients__r),
                                (select Id, Group_Event__c, Tag__c, Tag__r.Name, Required__c, Weighting__c from Group_Tags__r) 
                                from Group_Event__c where id=: GE.groupId];

        upGE.Id = Ge.groupId;
        upGE.Name = Ge.groupName;
        upGE.Group_Status__c = GE.groupStatus.id == 'none'?'':GE.groupStatus.label;
        upGE.Coordinator__c = GE.facilitator == null?null:GE.facilitator.id;
        upGE.Description__c = GE.description;
        //upGE.Attendance_Fee__c = GE.attendanceFee;
        upGE.GeoLocation__latitude__s = GE.location.lat;
        upGE.GeoLocation__longitude__s = GE.location.lng;
        upGE.Address__c = GE.location.address;
        //upGe.Alternative_Address__c = GE.alternativeLocation==null?'':GE.alternativeLocation.address;
        upGE.Region__c =  GE.region.id == 'none'?'':GE.region.id;
        if(GE.altAddressLookup == 'Account'){
            upGE.Account__c = string.isBlank(GE.account.id)?null:GE.account.id;
            upGE.Location__c    = null;
            upGE.Site__c        = null;
        }else {
            if(GE.altAddressLookup == 'Location'){
                upGE.Location__c    = string.isBlank(GE.site.id)?null:GE.site.id;
                upGE.Account__c     = null;
                upGE.Site__c        = null;
            } else if(GE.altAddressLookup == 'Site'){
                upGE.Site__c = string.isBlank(GE.site.id)?null:GE.site.id;
                upGE.Account__c     = null;
                upGE.Location__c    = null;
            }
        }

        upGE.Number_of_Resources_Required__c = GE.numberOfResourcesRequired;
        upGE.Maximum_Number_of_Participants__c = GE.maximumNumberOfParticipants;
        
        System.debug(upGE);
        Savepoint sp = Database.setSavepoint();
        try{
            update upGE;

            map<string,string> originalClients = new map<string,string>();
            for(Group_Client__c GEC : upGE.Group_Clients__r ){
                originalClients.put(GEC.Client__c,GEC.id);
            }

            list<Group_Client__c> insClients= new list<Group_Client__c>();
            list<Group_Client__c> updateClients= new list<Group_Client__c>();
            list<Group_Client__c> delClients= new list<Group_Client__c>();
            //Update for SEP-148
            Set<String> setDelClient = new Set<String>();

            Map<string, skedGroupClient> upClients= new map<string, skedGroupClient>();

            for(skedGroupClient BSP:GE.listGroupClients){
                if (!originalClients.containsKey(BSP.id)){
                    Group_Client__c GEP = new Group_Client__c(Group_Event__c = upGE.id,
                            Client__c = BSP.id,
                            Service_Agreement_Item__c = !string.isBlank(BSP.serviceAgreementItem)?BSP.serviceAgreementItem:BSP.matchingItem,
                            Service__c = BSP.service!=null?BSP.service.id:null,
                            Rate__c = BSP.rate==null?null:BSP.rate.id,
                            Program__c = BSP.program==null?null:BSP.program.id,
                            Site__c = BSP.site==null?null:BSP.site.id,
                            Delivery_Method__c = BSP.deliveryMethod,
                            Quantity__c = BSP.quantity
                    );
                    insClients.add(GEP);
                }
                else
                {
                    upClients.put(BSP.id, BSP);
                }
            }

            for(Group_Client__c GEC : upGE.Group_Clients__r ){
                if (!upClients.containsKey(GEC.Client__c))
                {
                    //update for SEP-148
                    setDelClient.add(GEC.Client__c);
                    delClients.add(GEC);
                    continue;
                }

                skedGroupClient groupClient = upClients.get(GEC.Client__c);
                GEC.Service_Agreement_Item__c = !string.isBlank(groupClient.serviceAgreementItem)?groupClient.serviceAgreementItem:groupClient.matchingItem;
                GEC.Service__c = groupClient.service==null?null:groupClient.service.id;
                GEC.Rate__c = groupClient.rate==null?null:groupClient.rate.id;
                GEC.Program__c = groupClient.program==null?null:groupClient.program.id;
                GEC.Site__c = groupClient.site==null?null:groupClient.site.id;
                GEC.Delivery_Method__c = groupClient.deliveryMethod;
                GEC.Quantity__c = groupClient.quantity;

                updateClients.add(GEC);
            }

            //update for SEP-148, remove the tag of del client
            Set<String> setDelTags = new Set<String>();
            for (Client_Tag__c tag : [SELECT Id, Tag__c FROM Client_Tag__c WHERE Client__c IN : setDelClient]) {
                setDelTags.add(tag.Tag__c);
            }

            insert insClients;
            update updateClients;
            delete delClients;

            list<Group_Tag__c> insTags= new list<Group_Tag__c>();
            list<Group_Tag__c> updateTags= new list<Group_Tag__c>();
            list<Group_Tag__c> delTags= new list<Group_Tag__c>();

            Set<String> originalTags = new Set<string>();
            for(Group_Tag__c gTag : upGE.Group_Tags__r ){
                originalTags.add(gTag.Tag__c);
                if (setDelTags.contains(gTag.Tag__c)) {
                    delTags.add(gTag);
                }
            }

            Map<string, skedTag> upTags= new map<string, skedTag>();

            for( skedTag tag : GE.tags ) {
                if (!originalTags.contains(tag.id)){
                    Group_Tag__c gTag = new Group_Tag__c(
                        Tag__c = tag.id,
                        Group_Event__c = upGE.id,
                        Required__c = tag.required,
                        Weighting__c = tag.required ? null : tag.weighting
                        );
                    insTags.add(gTag);
                }
                else
                {
                    upTags.put(tag.id, tag);
                }
            }

            for( Group_Tag__c gTag : upGE.Group_Tags__r ){
                if (!upTags.containsKey(gTag.Tag__c))
                {
                    delTags.add(gTag);
                    continue;
                }

                skedTag updatedTag = upTags.get(gTag.Tag__c);
                gTag.Weighting__c = updatedTag.required ? null : updatedTag.weighting;
                gTag.Required__c = updatedTag.required;                    

                updateTags.add(gTag);
            }
            System.debug('updateTags==' + updateTags);
            insert insTags;
            update updateTags;
            delete delTags;
            System.debug('saveTagToFutureJob==' + GE.saveTagToFutureJob);
            if ( GE.saveTagToFutureJob != NULL && GE.saveTagToFutureJob ) {
                List<Group_Tag__c> allGroupTags = new List<Group_Tag__c>();
                allGroupTags.addAll(insTags);
                allGroupTags.addAll(updateTags);
                addTagsToJob(upGE.Id, allGroupTags);
            }  
                      

        }catch (exception ex){
            Database.rollback(sp);
            return 'exception:'+getExceptionMessage(ex);
        }

        return upGE.id;
    }

    @remoteAction
    public static List<skedTag> searchGroupTags(String searchingText) {
        List<skedTag> models = new List<skedTag>();
        List<sked__Tag__c> tags = [SELECT Id, Name FROM sked__Tag__c WHERE Name LIKE :('%' + searchingText + '%')];

        for ( sked__Tag__c tag : tags ) {
            skedTag model = new skedTag(tag);
            models.add(model);
        }

        return models;
    }

    @remoteAction
    public static string createGroupEvent(skedGroupEvent GE){
        Group_Event__c newGE = new Group_Event__c(
            Name = Ge.groupName,
            Group_Status__c = GE.groupStatus.id == 'none'?null:GE.groupStatus.label,
            Coordinator__c = GE.facilitator == null?null:GE.facilitator.id,
            Description__c = GE.description,
            Service__c = (GE.serviceItem == null || string.isBlank(GE.serviceItem.id))?null:GE.serviceItem.id,
            Region__c =  GE.region.id == 'none'?null:GE.region.id,
            Account__c = (GE.account==null || string.isBlank(GE.account.id))?null:GE.account.id,
            Number_of_Resources_Required__c = GE.numberOfResourcesRequired,
            Maximum_Number_of_Participants__c = GE.maximumNumberOfParticipants
        );
        if(GE.location != null && String.isNotBlank(GE.location.address)){
            newGE.Address__c = GE.location.address;
        }
        if(GE.altAddressLookup == 'Account'){
            newGE.Account__c = string.isBlank(GE.account.id)?null:GE.account.id;
            newGE.Location__c    = null;
            newGE.Site__c        = null;
        }else {
            if(GE.altAddressLookup == 'Location'){
                newGE.Location__c    = string.isBlank(GE.site.id)?null:GE.site.id;
                newGE.Account__c     = null;
                newGE.Site__c        = null;
            } else if(GE.altAddressLookup == 'Site'){
                newGE.Site__c = string.isBlank(GE.site.id)?null:GE.site.id;
                newGE.Account__c     = null;
                newGE.Location__c    = null;
            }
        }
        //Attendance_Fee__c = GE.attendanceFee,
            
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug('new Group Event ' + newGE);
            insert newGE;
            list<Group_Client__c> insClients= new list<Group_Client__c>();
            for(skedGroupClient GC:GE.listGroupClients){
                Group_Client__c GEP  = new Group_Client__c(Group_Event__c = newGE.id,
                        Client__c = GC.id,
                        Service_Agreement_Item__c = !string.isBlank(GC.serviceAgreementItem)?GC.serviceAgreementItem:GC.matchingItem,
                        Service__c = GC.service!=null?GC.service.id:null,
                        Rate__c = GC.rate==null?null:GC.rate.id,
                        Program__c = GC.program==null?null:GC.program.id,
                        Site__c = GC.site==null?null:GC.site.id,
                        Delivery_Method__c = GC.deliveryMethod,
                        Quantity__c = GC.quantity
                );
                insClients.add(GEP);
            }
            insert insClients;

            list<Group_Tag__c> insTags= new list<Group_Tag__c>();
            for( skedTag gTag : GE.tags ) {
                Group_Tag__c insTag = new Group_Tag__c(
                    Tag__c = gTag.id,
                    Group_Event__c = newGE.Id,
                    Required__c = gTag.required,
                    Weighting__c = gTag.required ? null : gTag.weighting
                );
                insTags.add(insTag);
            }

            insert insTags;
                
        }catch (exception ex){
            Database.rollback(sp);
            return 'exception:' + getExceptionMessage(ex);
        }

        return newGE.id;
    }

    @remoteAction
    public static string updateAttendees(list<skedGroupClient> newAtteedees, string jobId){
        system.debug('Nam Le newAtteedees = ' + newAtteedees);
        Savepoint sp = Database.setSavepoint();
        try {
            list<Group_Attendee__c> attendees = [select id, Account__r.Name, Contact__c, Contact__r.Name from Group_Attendee__c where Job__c = :jobId];
            Map<Id, Group_Attendee__c> oldMap = new Map<Id, Group_Attendee__c>();
            Map<Id, Group_Attendee__c> newMap = new Map<Id, Group_Attendee__c>();

            for(skedGroupClient att : newAtteedees)
            {
                newMap.put(att.id, new Group_Attendee__c(Account__c = att.accId, Contact__c = att.id, Job__c = jobId));
            }

            for(Group_Attendee__c att : attendees)
            {
                oldMap.put(att.Contact__c, att);
            }

            List<Group_Attendee__c> insertGroupAttendees = new List<Group_Attendee__c>();
            List<Group_Attendee__c> deleteGroupAttendees = new List<Group_Attendee__c>();
            for(Group_Attendee__c attendee : newMap.values())
            {
                if(!oldMap.containsKey(attendee.Contact__c))
                {
                    insertGroupAttendees.add(attendee);
                }
            }

            for(Group_Attendee__c attendee : oldMap.values())
            {
                if(!newMap.containsKey(attendee.Contact__c))
                {
                    deleteGroupAttendees.add(attendee);
                }
            }

            insert insertGroupAttendees;
            delete deleteGroupAttendees;


            return 'Successful';
        } catch (exception ex) {
            Database.rollback(sp);


            return 'exception:'+getExceptionMessage(ex);
        }

        return 'Failed';
    }

    public static String getExceptionMessage(Exception e)
    {
        String msg = e.getMessage();
        if(msg.contains('insufficient access rights on object id'))
        {
            return 'You cannot edit this record.';
        }
        if(e instanceof DMLException)
        {
            String message = ((DMLException)e).getDmlMessage(0);
            if (String.isNotBlank(message) && message.contains(FIELD_CUSTOM_VALIDATION_EXCEPTION)) {
                Integer index = message.lastIndexOf(FIELD_CUSTOM_VALIDATION_EXCEPTION) + FIELD_CUSTOM_VALIDATION_EXCEPTION.length();
                Integer indexBrace = message.indexOf('[');
                if (index < indexBrace && message.length() > indexBrace) {
                    message = message.substring(index, indexBrace).removeEnd(': ');
                }
            }
            return message;
        }
        return msg;
    }

    public class skedManageAttendee {
        public string id {get;set;}
        public string name {get;set;}
        public string contactId {get;set;}
        public string account {get;set;}

        public skedManageAttendee(string i, string n, string c, string a){
            id = i;
            name = n;
            contactId = c;
            account = a;
        }

    }

    public static void addTagsToJob(Id groupEventId, List<Group_Tag__c> groupTags) {
        Map<Id, Group_Tag__c> mpGroupTag = new Map<Id, Group_Tag__c>();
        for ( Group_Tag__c gTag : groupTags ) {
            mpGroupTag.put(gTag.Tag__c, gTag);
        }

        Set<String> ignoredJobStatuses = new Set<String>{SkeduloConstants.JOB_STATUS_COMPLETE, SkeduloConstants.JOB_STATUS_CANCELLED};
        DateTime currentTime = System.now();
        List<sked__Job__c> jobs = [SELECT Id, 
                                    (SELECT Id, sked__Job__c, sked__Tag__c, sked__Required__c, sked__Weighting__c FROM sked__JobTags__r)
                                    FROM sked__Job__c
                                    WHERE sked__Job_Status__c NOT IN :ignoredJobStatuses
                                    AND Group_Event__c = :groupEventId
                                    AND sked__Start__c > :currentTime];
        System.debug('jobs==' + jobs);
        List<sked__Job_Tag__c> jobTags = new List<sked__Job_Tag__c>();
        for ( sked__Job__c job : jobs ) {
            Set<Id> jobTagIds = new Set<Id>();
            for ( sked__Job_Tag__c jobTag : job.sked__JobTags__r ) {
                jobTagIds.add(jobTag.sked__Tag__c);
            }
            for ( Id tagId : mpGroupTag.keySet() ) {
                if ( !jobTagIds.contains(tagId) ) {
                    sked__Job_Tag__c jobTag = new sked__Job_Tag__c(
                        sked__Tag__c = tagId,
                        sked__Job__c = job.Id,
                        sked__Required__c = mpGroupTag.get(tagId).Required__c,
                        sked__Weighting__c = mpGroupTag.get(tagId).Required__c ? null : mpGroupTag.get(tagId).Weighting__c
                    );

                    jobTags.add(jobTag);
                }
            }
        }
        System.debug('jobTags==' + jobTags);
        if ( jobTags.size() >0 ) insert jobTags;                            
    }

}