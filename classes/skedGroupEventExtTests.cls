@isTest
private class skedGroupEventExtTests {

	@isTest static void TestGeneralMethods() {
		skedDataFactory.createRegions();
		Account acc = skedDataFactory.createAccount();
		Contact con = skedDataFactory.createEmployee(acc);
		Contact client = skedDataFactory.createClient(acc, 'TesClient');
		enrtcr__Service__c serviceItem  = skedDataFactory.createServiceItem('Test Service');
		Test.startTest();
			skedGroupEventExt.getAllRegions();
			skedGroupEventExt.getSkedGroupEvent('','');
			skedGroupEventExt.searchFacilitator('Tes');
			skedGroupEventExt.searchAccount('Tes');
			skedGroupEventExt.searchServiceItem('Tes');
			skedGroupEventExt.searchClients('Tes');
		Test.stopTest();
	}

    @isTest static void TestInsertGroupAttendee() {
         Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');
        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');

        Group_Attendee__c groupAtendee = new Group_Attendee__c(Job__c=job.id, Contact__c=client.id);
        insert groupAtendee;
    }
	
	@isTest static void TestCreateJobSaveGroup()
    {

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);


        Test.startTest();
        skedGroupEventExt.saveGroupEvent(skedGE);
        Test.stopTest();
    }


	@isTest static void TestupdateAttendees()
	{
        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
               acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


		skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
		skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
		skedGE.startTime = Datetime.now().format('HH:mm');
		skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);


        Test.startTest();

		Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
		insert att;

		att.Note__c = 'Test Note';
		update att;

		skedGroupEventExt.updateAttendees(newAtteedees, job.Id);

		Test.stopTest();
	}


	@isTest static void testSearchAtteedees()
	{

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);

		Test.startTest();
		Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
		insert att;

		att.Note__c = 'Test Note';
		update att;

		skedGroupEventExt.searchAtteedees(job.Id);

		Test.stopTest();
	}


	@isTest static void testCreateGroupEvent()
	{

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        sked__Tag__c tag = new sked__Tag__c(Name = 'Communication');
        insert tag;

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);
        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);
		Test.startTest();
		Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
		insert att;

		att.Note__c = 'Test Note';
		update att;

		skedGroupEventExt.createGroupEvent(skedGE);

        skedTag st = new skedTag(tag);
        st.required = true;

        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});

        skedGroupEventExt.searchGroupTags('comm');
        skedGroupEventExt.addTagsToJob(Ge.Id, new list<Group_Tag__c>{gt});

		Test.stopTest();
	}


	@isTest static void testSearchClientsWithActiveServiceAgreements()
	{

		List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
		Account acc = skedDataFactory.createAccount();
		Contact con = skedDataFactory.createEmployee(acc);
		enrtcr__Support_Delivered__c serviceDelivered =  skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());

		Test.startTest();

		Contact testClient = [SELECT id, name from contact where id= :serviceDelivered.enrtcr__Client__c];

		Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, lstRegion[0], acc, testClient);

		list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
		skedGroupClient skedGC = new skedGroupClient();
		skedGC.accId = acc.id;
		skedGC.id = testClient.id;
		newAtteedees.add(skedGC);


		skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
		skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
		skedGE.startTime = Datetime.now().format('HH:mm');
		skedGE.duration = 60;

		skedGroupEventExt.searchClientsWithActiveServiceAgreements('Test', '');

		Test.stopTest();
	}

	@isTest static void testController()
	{

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c groupEvent = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');

		Test.startTest();
		skedGroupEventExt ctr = new skedGroupEventExt(new ApexPages.StandardController(groupEvent));
		ctr.getRecordName();

		skedGroupEventExt.skedManageAttendee attendee = new skedGroupEventExt.skedManageAttendee('a','b','c','d');
		skedGroupEventExt.cancelJob(groupEvent.Id,job.Id,'Test');
		Test.stopTest();
	}

    @isTest static void testInsertServiceDelivery()
    {

        enrtcr__Support_Delivered__c support_delivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
    }
}