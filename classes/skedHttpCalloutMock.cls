public class  skedHttpCalloutMock  implements HttpCalloutMock{
    
    public static String GEOCODE_DATA = '{"results":[{"address_components":[{"long_name":"StationStreet","short_name":"StationSt","types":["route"]},{"long_name":"Sunbury","short_name":"Sunbury","types":["locality","political"]},{"long_name":"HumeCity","short_name":"Hume","types":["administrative_area_level_2","political"]},{"long_name":"Victoria","short_name":"VIC","types":["administrative_area_level_1","political"]},{"long_name":"Australia","short_name":"AU","types":["country","political"]},{"long_name":"3429","short_name":"3429","types":["postal_code"]}],"formatted_address":"StationSt&EvansSt,SunburyVIC3429,Australia","geometry":{"location":{"lat":-37.58204540000001,"lng":144.7276305},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":-37.58069641970851,"lng":144.7289794802915},"southwest":{"lat":-37.58339438029151,"lng":144.7262815197085}}},"place_id":"EjJTdGF0aW9uIFN0ICYgRXZhbnMgU3QsIFN1bmJ1cnkgVklDIDM0MjksIEF1c3RyYWxpYQ","types":["intersection"]}],"status":"OK"}';
    public static String OPTIMIZING_DATA = '{"result":{"routes":[{"resourceId":"a0L0k0000004QmIEAU","resourceName":"Sked Tester","route":[{"jobId":"a0E0k0000005cBMEAY","jobName":"JOB-30259","start":"2017-05-09T10:45:00.000+10:00","duration":30,"travelTime":30,"type":"job"},{"jobId":"a0E0k0000005c9QEAQ","jobName":"JOB-30255","start":"2017-05-09T11:15:00.000+10:00","duration":30,"travelTime":0,"type":"job"},{"jobId":"a0E0k0000005c9VEAQ","jobName":"JOB-30256","start":"2017-05-09T12:00:00.000+10:00","duration":30,"travelTime":0,"type":"job"},{"jobId":"a010k000000TBmDAAW","jobName":"ACT-0108","start":"2017-05-09T15:45:00.000+10:00","duration":60,"travelTime":473,"type":"activity"}]}],"unscheduled":[{"jobId":"a0E0k0000005cDmEAI","jobName":"JOB-30262","address":"Corner Evans Street and Station Street, Sunbury VIC 3429, Australia.","jobReasons":[],"resourceReasons":{"a0L0k0000004QmIEAU":{"name":"Sked Tester","address":"70 Tunaley Parade Reservoir VIC 3073","reasons":["Cannot find large enough availability for this job in the current schedule / scheduling window. Home to job: 33 mins, Job to home: 32 mins, duration: 15 mins."]}}},{"jobId":"a0E0k0000005cBbEAI","jobName":"JOB-30260","address":"780 Sunbury Rd Sunbury VIC 3429, Australia.","jobReasons":[],"resourceReasons":{"a0L0k0000004QmIEAU":{"name":"Sked Tester","address":"70 Tunaley Parade Reservoir VIC 3073","reasons":["Cannot find large enough availability for this job in the current schedule / scheduling window. Home to job: 30 mins, Job to home: 30 mins, duration: 30 mins."]}}}]}}';
    public static String DISTANCE_MATRIX_DATA = '{"destination_addresses":["SanFrancisco,CA,USA","Victoria,BC,Canada"],"origin_addresses":["Vancouver,BC,Canada","Seattle,WA,USA"],"rows":[{"elements":[{"distance":{"text":"1,529km","value":1528699},"duration":{"text":"14hours56mins","value":53778},"status":"OK"},{"distance":{"text":"115km","value":114513},"duration":{"text":"3hours14mins","value":11614},"status":"OK"}]},{"elements":[{"distance":{"text":"1,300km","value":1299985},"duration":{"text":"12hours25mins","value":44695},"status":"OK"},{"distance":{"text":"297km","value":296838},"duration":{"text":"4hours48mins","value":17285},"status":"OK"}]}],"status":"OK"}';
    public static String DISPATCH_API_DATA = '{"results":{"abcdef":{"dts":"2016-04-22T06:37:59.585Z","protocol":"n/a","success":true}}}';
    public String data;

    public skedHttpCalloutMock(String d){
        this.data   = d;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
        res.setBody(this.data);
        res.setStatusCode(200);
        
        return res;
    }
}