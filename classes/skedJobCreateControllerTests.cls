@isTest
private class skedJobCreateControllerTests {
    
    @isTest static void CreatejobWithServiceItem() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc,'Test');
        enrtcr__Support_Contract_Item__c SupContract = skedDataFactory.createSupportContract(client);
        sked__Location__c location = skedTestDataFactory.createLocation('Test', acc.Id, lstRegion.get(0).Id);
        insert location;

        ApexPages.StandardController std = new ApexPages.StandardController(client);
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement_Item_Temp__c = SupContract.id;

        Test.startTest();
        cont.Cmd_PopulateAddress();
        cont.jobStartDateString = system.today().format();
        system.debug(cont.ServiceAgreementItems);
        system.debug(cont.CategoryRestrictToItems);
        system.debug(cont.SAStartDate);
        system.debug(cont.SAEndDate);

        cont.altAddressLookup = 'Location';
        cont.DummyJob.sked__Location__c = location.Id;
        cont.Cmd_PopulateAddress();

        cont.Cmd_Confirm();
        Test.stopTest();
    }
    
    @isTest static void CreatejobWithService() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.Cmd_Confirm();
        cont.jobStartDateString = system.today().format();
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();
        cont.Cmd_PopulateAddress();
        
        cont.Cmd_Confirm();

        cont.Cmd_PopulateCategories();
        
        cont.NewJob.sked__Job_Status__c = 'Dispatched';
        upsert cont.newJob;
        
        Test.stopTest();
    }

    @isTest static void CreatejobWithServiceAgreementItem() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc,'Test');
        enrtcr__Support_Contract_Item__c SupContract = skedDataFactory.createSupportContract(client);

        ApexPages.StandardController std = new ApexPages.StandardController(client);
        skedJobCreationController cont = new skedJobCreationController(std);
         Test.startTest();
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement_Item_Temp__c = SupContract.id;
        cont.jobStartDateString = datetime.now().format('dd/MM/YYYY');

       
        cont.Cmd_SelectedCategory();
        cont.Cmd_PopulateCategories();
        cont.NewJob.Service__c = SupContract.enrtcr__Service__c;
        cont.Cmd_SelectedService();
        cont.Cmd_SelectProgram();
        cont.Cmd_SelectServiceAgreementItemCategory();

        Test.stopTest();



    }

    @isTest static void UpdateCompleteJobActualFinishTime() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.Cmd_Confirm();
        cont.jobStartDateString = system.today().format();
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();
        cont.Cmd_PopulateAddress();
        
        cont.Cmd_Confirm();

        cont.Cmd_PopulateCategories();
        
        cont.NewJob.sked__Job_Status__c = 'Dispatched';
        cont.NewJob.sked__Start__c = System.now();
        cont.NewJob.sked__Finish__c = System.now().addHours(1);
        upsert cont.newJob;



        //cont.newJob.sked__Job_Status__c = 'Complete';
        //update cont.newJob;
        System.debug('cont.newJob = ' + cont.newJob);
/*
        DateTime jobFinishTime = cont.newJob.sked__Actual_End__c;
        DateTime newActualJobFinishTime = jobFinishTime.addDays(1);
        cont.newJob.sked__Actual_End__c = newActualJobFinishTime;
        update cont.newJob;
*/
        
        Test.stopTest();
    }

    @isTest static void confirmRegion() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);

        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
        from contact where id =:supportDelivered.enrtcr__Client__c];

        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.Region_tmp__c = lstRegion[0].id;
        cont.AlternativeAddress = 'Alt address';
        cont.Cmd_Confirm();
        cont.NewJob.Service_Agreement_Item__c = null;
        cont.SelectedServiceAgreementItem = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();

        Test.stopTest();
    }



}