public class skedJobCreationController
{

    public Contact MainContact { get; set; }

    public List<SelectOption> JobTypeOptions { get; set; }
    public List<SelectOption> UrgencyOptions { get; set; }

    private Map<String, List<enrtcr.ListOption.ServiceAgreementCategoryListOption>> servicesForClientMap { get; set; }
    public Map<string, enrtcr.ListOption.UnrestrictedSiteListOption> sites { get; set; }
    public List<enrtcr.ListOption.ServiceAgreementItemMatchListOption> ServiceAgreementItemsCategories { get; set; }

    public List<SelectOption> Categories { get; set; }
    public List<SelectOption> ServiceSites { get; set; }
    public List<SelectOption> Programs { get; set; }
    public List<SelectOption> SaiCategories { get; set; }
    public List<SelectOption> Rates { get; set; }

    public String SelectedCategory { get; set; }
    public String SelectedServiceAgreementItem { get; set; }
    public String SelectedSite { get; set; }
    public String SelectedProgram { get; set; }

    public String locationOrSiteFilter{
        get{
            return skedConfigs.LOCATION_OR_SITE_FILTER;
        }
    }

    public String jobURL{
        get{
            return skedConfigs.JOB_URL;
        }
    }

    public String altAddressLookup{
        get{
            if(altAddressLookup==null) altAddressLookup = 'Account';
            return altAddressLookup;
        }
        set;
    }

    public list<SelectOption> getAltLookupOptions(){
        list<SelectOption> options = new list<SelectOption>();
        options.add( new SelectOption(locationOrSiteFilter, locationOrSiteFilter) );
        options.add( new SelectOption('Account', 'Account') );
        return options;
    }

    public void changeAltAddressLookup(){
        
    }

    @AuraEnabled
    public static string getVisualforceUrl(){
        string base =  URL.getSalesforceBaseUrl().toExternalForm();
        String instanceName = [select InstanceName from Organization limit 1].InstanceName;


        return base.replace('.my.sales', '--c.'+instanceName.toLowerCase()+'.visual.');
    }


    public List<SelectOption> ServiceAgreementItems
    {
        get
        {
            if (this.ServiceAgreementItems == null)
            {
                this.ServiceAgreementItems = new List<SelectOption>();
            }

            return this.ServiceAgreementItems;
        }
        set;
    }

    public Boolean CategoryRestrictToItems
    {
        get
        {
            try
            {
                if (string.isBlank(SelectedCategory))
                    return false;

                for (enrtcr.ListOption.ServiceAgreementCategoryListOption option : servicesForClientMap.get(SelectedCategory))
                {
                    if (!option.IsCategory) continue;

                    if (!option.RestrictToItems) return false;
                }
                return true;
            }
            catch(Exception ex)
            {
                system.debug(this.SelectedCategory+'cat');
                return false;
            }
        }
    }


    public Map<string, List<enrtcr__Support_Contract_Item__c>> MapServiceAgreements { get; set; }

    public sked__Job__c NewJob { get; set; }
    public sked__Job__c DummyJob { get; set; }
    public string AlternativeAddress { get; set; }
    public string jobStartDateString { get; set; }
    public string jobStartTimeString { get; set; }

    public boolean SaveCompleted { get; set; }
    public string SAStartDate { get; set; }
    public string SAEndDate { get; set; }

    public skedJobCreationController(ApexPages.StandardController stdCon)
    {
        string contactId = stdCon.getID();
        if (!string.isBlank(contactId))
        {
            Initialize(contactId);
        }
        this.JobTypeOptions = getPickListValues('sked__Job__c', 'sked__Type__c');
        this.UrgencyOptions = getPickListValues('sked__Job__c', 'sked__Urgency__c');
        this.SaveCompleted = false;
        this.DummyJob = new sked__Job__c();
        NewJob.Delivery_Method__c = skedConfigs.DEFAULT_DELIVERY_METHOD;
        
        String startTime = ApexPages.currentPage().getParameters().get('startTime');
        if(!String.isBlank(startTime)){
            jobStartTimeString = startTime;
        }
        String duration = ApexPages.currentPage().getParameters().get('duration');
        if(!String.isBlank(duration)){
            NewJob.sked__Duration__c = Integer.valueOf(duration);
        }
        String startDate = ApexPages.currentPage().getParameters().get('startDate');
        if(!String.isBlank(startDate)){
            jobStartDateString = startDate;
            Cmd_PopulateCategories();
        }
        String regionId = ApexPages.currentPage().getParameters().get('regionId');
        if(!String.isBlank(regionId)){
            list<sked__Region__c> regions = [Select Id from sked__Region__c where Id=:regionId];
            if(!regions.isEmpty()) NewJob.Region_tmp__c = regions.get(0).Id;
        }
        String locationId = ApexPages.currentPage().getParameters().get('locationId');
        if(!String.isBlank(locationId)){
            this.altAddressLookup = locationOrSiteFilter;
            if(locationOrSiteFilter=='Location'){
                DummyJob.sked__Location__c = locationId;
            }else{
                DummyJob.Site__c = locationId;
            }
            Cmd_PopulateAddress();
        }
    }

    boolean validateJobStarDate(Date jobStartDate, Date serviceAgreementStartDate, Date serviceAgreementEndDate)
    {
        if (jobStartDate >= serviceAgreementStartDate && jobStartDate <= serviceAgreementEndDate)
            return true;
        return false;
    }

    public void Cmd_Confirm()
    {
        try
        {
            if (string.isBlank(jobStartDateString))
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Start Date: You must enter a valid date.'));

                this.SaveCompleted = false;
                return;
            }

            if (string.isBlank(jobStartTimeString))
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Start Time: You must enter a valid time.'));
                this.SaveCompleted = false;
                return;
            }

            if(NewJob.Region_tmp__c == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Region: You must select a region.'));
                this.SaveCompleted = false;
                return;
            }
            else {
                NewJob.sked__Region__c = NewJob.Region_tmp__c;
            }

            if(NewJob.Delivery_Method__c == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Delivery Method: You must select a delivery method.'));
                this.SaveCompleted = false;
                return;
            }
            else if(NewJob.Delivery_Method__c == SkeduloConstants.DELIVERY_METHOD_STATUS_FIXED_QUANTITY && NewJob.Delivery_Method_Quantity__c == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Quantity: You must enter a value when delivery method is fixed quantity.'));
                this.SaveCompleted = false;
                return;

            }

            if (!string.isBlank(this.AlternativeAddress))
            {
                this.NewJob.sked__Address__c = this.AlternativeAddress;
            }
            
            if ( String.isBlank(this.NewJob.sked__Address__c) )
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Address: You must enter an address'));
                this.SaveCompleted = false;
                return;
            }
            
            if (SelectedServiceAgreementItem == null && NewJob.Service_Agreement_Item__c == null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a Service Agreement Item or Category prior to Job Schedule.'));
                this.SaveCompleted = false;
                return;
            }


            if(NewJob.Service_Agreement_Item__c == null)
            {
                NewJob.Service_Agreement_Item__c = SelectedServiceAgreementItem;
            }

            if (NewJob.Service_Agreement_Item__c != null)
            {

                List<enrtcr__Support_Contract_Item__c > items =
                [
                        SELECT Id, enrtcr__Support_Contract__c, enrtcr__Support_Contract__r.enrtcr__Start_Date__c,
                                enrtcr__Support_Contract__r.enrtcr__End_Date__c
                        FROM enrtcr__Support_Contract_Item__c
                        WHERE Id = :NewJob.Service_Agreement_Item__c
                ];

                if (items != null && items.size() > 0)
                {
                    this.NewJob.Service_Agreement__c = items[0].enrtcr__Support_Contract__c;

                    this.SAStartDate = items[0].enrtcr__Support_Contract__r.enrtcr__Start_Date__c.format();
                    this.SAEndDate = items[0].enrtcr__Support_Contract__r.enrtcr__End_Date__c.format();
                }
            }

            boolean validateResult = validateJobStarDate(Date.parse(jobStartDateString), Date.parse(this.SAStartDate), Date.parse(this.SAEndDate));
            if (validateResult == FALSE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,
                        'Job Start Date cannot be outside of the Start Date (' + this.SAStartDate + ') and End Date (' + this.SAEndDate + ') of the Service Agreement.'));
            }

            if (this.NewJob.Service_Agreement__c != NULL && this.NewJob.Service_Agreement_Item__c == NULL)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a Funded Service Item prior to Job Schedule.'));
                validateResult = FALSE;
            }

            this.SaveCompleted = validateResult;
            if (this.SaveCompleted)
            {
                Contact cont = [SELECT Id, Name FROM Contact Where Id = :this.NewJob.sked__Contact__c];
                if (!string.isBlank(this.NewJob.sked__Description__c))
                {
                    this.NewJob.sked__Description__c = cont.Name + ' - ' + this.NewJob.sked__Description__c;
                }
                else
                {
                    this.NewJob.sked__Description__c = cont.Name;
                }

                this.NewJob.sked__Start__c = DateTime.newInstance(Date.parse(jobStartDateString), getStartTime());
                this.NewJob.sked__Finish__c = this.NewJob.sked__Start__c.addMinutes(integer.valueOf(this.NewJob.sked__Duration__c));
                this.NewJob.Service_Agreement_Item__c = SelectedServiceAgreementItem;
                this.NewJob.Site__c = string.isBlank(SelectedSite)?null:SelectedSite;
                this.NewJob.sked__Type__c = 'Single Booking';
                if(altAddressLookup=='Location' && !String.isBlank(DummyJob.sked__Location__c)){
                    NewJob.sked__Location__c = DummyJob.sked__Location__c;
                    NewJob.Site__c = null;
                }else if(altAddressLookup=='Site' && !String.isBlank(DummyJob.Site__c)){
                    NewJob.Site__c = DummyJob.Site__c;
                    NewJob.sked__Location__c = null;
                }
                //this.NewJob.Rate__c = s
                //this.NewJob.Service__c = string.isBlank(selected)

                insert this.NewJob;
                this.SaveCompleted = true;


                system.debug(this.NewJob.Id);
            }
        }
        catch (Exception ex)
        {
            this.SaveCompleted = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString()));
        }
    }

    public Time getStartTime()
    {
        boolean isAfterNoon = false;
        if(jobStartTimeString.indexOf('PM') > - 1 || jobStartTimeString.indexOf('pm') > -1){
            isAfterNoon = true;
        }
        integer hour    = Integer.valueOf(jobStartTimeString.split(':')[0]);
        if(hour == 12 && jobStartTimeString.indexOf('am') > -1 ) hour = 0;
        integer minute  = Integer.valueOf(jobStartTimeString.split(':')[1].remove('AM').remove('am').remove('PM').remove('pm').trim());
        if(isAfterNoon && hour != 12) hour = hour + 12;

        return time.newInstance(hour, minute, 0, 0);
    }

    public void Cmd_PopulateAddress()
    {
        this.AlternativeAddress = '';
        if(altAddressLookup == 'Account' && !String.isBlank(this.DummyJob.sked__Account__c)){
            Account acc = [ SELECT Id, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry 
                                        FROM Account WHERE Id = :this.DummyJob.sked__Account__c LIMIT 1];
            if (string.isNotBlank(acc.ShippingStreet)){
                this.AlternativeAddress = skedUtils.CombineAddress(acc.ShippingStreet, acc.ShippingCity, acc.ShippingState, acc.ShippingPostalCode, acc.ShippingCountry);
            }
        }else if(altAddressLookup == 'Location' && !String.isBlank(this.DummyJob.sked__Location__c)){
            sked__Location__c location = [Select Id, sked__Address__c, sked__Region__c from sked__Location__c where Id=:DummyJob.sked__Location__c];
            this.AlternativeAddress = location.sked__Address__c;
            //Populate Region
            if(NewJob.Region_tmp__c==null){
                NewJob.Region_tmp__c = location.sked__Region__c;
            }
        }else if(altAddressLookup == 'Site' && !String.isBlank(this.DummyJob.Site__c)){
            enrtcr__Site__c site = [Select Id, Address__c from enrtcr__Site__c where Id=:DummyJob.Site__c];
            this.AlternativeAddress = site.Address__c;
        }
        
    }

    public void Cmd_SelectedCategory()
    {
        NewJob.Service__c = null;
        SelectedServiceAgreementItem = null;
        NewJob.Service_Agreement_Item__c = null;
        SaiCategories = null;
        SelectedSite =null;
        Programs = null;
        ServiceAgreementItemsCategories = null;
        Rates = null;

        this.ServiceAgreementItems = new List<SelectOption>();
        if (!string.isBlank(SelectedCategory))
        {
            for (enrtcr.ListOption.ServiceAgreementCategoryListOption option: servicesForClientMap.get(SelectedCategory))
            {
                if (option.IsCategory) continue;

                this.ServiceAgreementItems.add(new SelectOption(option.Id, option.label));
            }

            if(this.ServiceAgreementItems.size() == 1)
            {
                NewJob.Service_Agreement_Item__c = ServiceAgreementItems[0].getValue();
            }
        }
        Cmd_SelectedService();
    }

    public void Cmd_SelectedService()
    {
        this.sites = new Map<string, enrtcr.ListOption.UnrestrictedSiteListOption>();
        this.ServiceSites = new List<SelectOption>();
        this.SelectedSite = null;
        SelectedServiceAgreementItem = null;
        NewJob.Rate__c = null;
        this.SelectedProgram = null;

        if (NewJob.Service__c != null)
        {
            enrtcr.LookupParams params = new enrtcr.LookupParams();
            params.additionalCriteria = new Map<string, string>
            {
                    'serviceId' => NewJob.Service__c,
                    'searchDate' => this.jobStartDateString
            };


            for (enrtcr.ListOption.UnrestrictedSiteListOption option : enrtcr.CustomRemoteActionController.lookupSites(params))
            {
                this.sites.put(option.id, option);
                this.ServiceSites.add(new SelectOption(option.id, option.label));
            }

            system.debug(sites);
            if (this.ServiceSites.size() > 0)
            {
                this.SelectedSite = this.ServiceSites[0].getValue();
                Cmd_SelectedSite();
            }
        }
    }

    public void Cmd_SelectedSite()
    {
        this.SelectedProgram = null;
        this.Programs = new List<SelectOption>();
        if (this.sites.get(SelectedSite).Programs.size() > 0)
        {
            for (enrtcr.ListOption.UnrestrictedProgramListOption programListOption : this.sites.get(SelectedSite).Programs)
            {
                this.Programs.add(new SelectOption(programListOption.id, programListOption.label));
            }
        }
        Cmd_SelectProgram();
    }

    public void Cmd_SelectProgram()
    {
        SelectedServiceAgreementItem = null;

        enrtcr__Support_Delivered__c serviceDelivery = new enrtcr__Support_Delivered__c(
                enrtcr__Site__c = SelectedSite,
                enrtcr__Program__c = string.isBlank(SelectedProgram) ? null : SelectedProgram,
                enrtcr__Adhoc_Service__c = NewJob.Service__c,
                enrtcr__Client__c = this.MainContact.Id,
                enrtcr__Date__c = Date.parse(this.jobStartDateString),
                enrtcr__Support_CategoryId__c = this.SelectedCategory
        );


        this.ServiceAgreementItemsCategories = enrtcr.ServicesDeliveredController.getMatchingItems(serviceDelivery);

        SaiCategories = new List<SelectOption>();
        for (enrtcr.ListOption.ServiceAgreementItemMatchListOption option : this.ServiceAgreementItemsCategories)
        {
            SaiCategories.add(new SelectOption(option.id, option.label));
        }

        if(SaiCategories.size() == 1)
        {
            SelectedServiceAgreementItem = SaiCategories[0].getValue();
            Cmd_SelectServiceAgreementItemCategory();
        }
    }

    public void Cmd_SelectServiceAgreementItemCategory()
    {
        NewJob.Rate__c = null;
        Rates = new List<SelectOption>();
        if(SelectedServiceAgreementItem == null) return;

        enrtcr__Support_Delivered__c sd = new enrtcr__Support_Delivered__c(
                enrtcr__Support_Contract_Item__c = SelectedServiceAgreementItem,
                enrtcr__Adhoc_Service__c = NewJob.Service__c,
                enrtcr__Date__c = Date.parse(this.jobStartDateString)
        );

        for(enrtcr.ListOption option : enrtcr.ServicesDeliveredController.getRatesForServiceAgreementItemCategory(sd))
        {
            Rates.add(new SelectOption(option.id, option.label));
        }
    }

    public void Cmd_PopulateCategories()
    {

        if (!string.isBlank(jobStartDateString))
        {
            try
            {
                this.servicesForClientMap = new Map<string, List<enrtcr.ListOption.ServiceAgreementCategoryListOption>>();
                this.SelectedCategory = null;

                String dateString = Datetime.parse(this.jobStartDateString + ' 09:00 AM').format('YYYY-MM-dd');
                List<enrtcr.ListOption.ServiceAgreementCategoryListOption> servicesForClient = enrtcr.CustomRemoteActionController.getServicesForClient(dateString, this.MainContact.Id, null);
                system.debug('===servicesForClient: ' + servicesForClient);
                for (enrtcr.ListOption.ServiceAgreementCategoryListOption serviceAgreementCategoryListOption: servicesForClient)
                {
                    if (serviceAgreementCategoryListOption.IsCategory)
                    {
                        servicesForClientMap.put(serviceAgreementCategoryListOption.key, new List<enrtcr.ListOption.ServiceAgreementCategoryListOption>());
                    }

                    string key = serviceAgreementCategoryListOption.IsCategory?serviceAgreementCategoryListOption.key:serviceAgreementCategoryListOption.parentKey;
                    servicesForClientMap.get(key).add(serviceAgreementCategoryListOption);
                }

                Map<string, SelectOption> availableCategories = new Map<string, SelectOption>();
                for (enrtcr.ListOption.ServiceAgreementCategoryListOption serviceAgreementCategoryListOption: servicesForClient)
                {
                    if(availableCategories.containsKey(serviceAgreementCategoryListOption.key)) continue;
                    if(serviceAgreementCategoryListOption.IsCategory && (servicesForClientMap.get(serviceAgreementCategoryListOption.key).size() > 1 || !serviceAgreementCategoryListOption.RestrictToItems))
                    {
                        availableCategories.put(serviceAgreementCategoryListOption.key, new SelectOption(serviceAgreementCategoryListOption.key, serviceAgreementCategoryListOption.value));
                    }
                }


                this.Categories = availableCategories.values();
                system.debug('===Categories: ' + this.Categories);

                if (servicesForClient.size() > 0)
                {
                    this.SelectedCategory = this.Categories[0].getValue();
                    Cmd_SelectedCategory();
                }
            }
            catch (Exception ex)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Start Date: You must enter a valid date.'));
            }
        }
    }

    private void Initialize(string contactId)
    {
        this.MainContact =
        [
                SELECT Id, Name, AccountId, OtherStreet, OtherCity, OtherPostalCode, OtherState
                FROM Contact
                WHERE Id = :contactId
                LIMIT 1
        ];

        List<enrtcr__Support_Contract__c> serviceAgreements =
        [
                SELECT Id, Name,
                (
                        SELECT Id, Name
                        FROM enrtcr__Support_Contract_Items__r
                )
                FROM enrtcr__Support_Contract__c
                WHERE enrtcr__Client__c = :contactId
        ];
        this.MapServiceAgreements = new Map<string, List<enrtcr__Support_Contract_Item__c>>();
        //this.MapServiceAgreements.put(null, new List<enrtcr__Support_Contract_Item__c>());
        for (enrtcr__Support_Contract__c sa : serviceAgreements)
        {
            this.MapServiceAgreements.put(sa.Name, sa.enrtcr__Support_Contract_Items__r);
        }

        this.NewJob = new sked__Job__c(
                sked__Account__c = this.MainContact.AccountId,
                sked__Contact__c = this.MainContact.Id,
                sked__Duration__c = 60
        );

        List<sked__Region__c> regionResults = [SELECT Id FROM sked__Region__c WHERE Name = :skedConstants.REGION_WESTERN_AUSTRALIA];
        if (regionResults != NULL && regionResults.size() > 0)
        {
            this.NewJob.Region_tmp__c = regionResults.get(0).Id;
        }

        if (string.isNotBlank(this.MainContact.OtherStreet))
        {
            string address = this.MainContact.OtherStreet;
            if (string.isNotBlank(this.MainContact.OtherCity))
            {
                address += ', ' + this.MainContact.OtherCity;
            }
            if (string.isNotBlank(this.MainContact.OtherState))
            {
                address += ', ' + this.MainContact.OtherState;
            }
            if (string.isNotBlank(this.MainContact.OtherPostalCode))
            {
                address += ' ' + this.MainContact.OtherPostalCode;
            }
            this.NewJob.sked__Address__c = address;
        }
        jobStartDateString = '';
        this.SaveCompleted = false;
    }


    private List<SelectOption> getPickListValues(string objectApiName, string fieldApiName)
    {
        List<SelectOption> results = new List<SelectOption>();

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        SObjectField fieldType = mapFields.get(fieldApiName);
        DescribeFieldResult fieldResult = fieldType.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry f : ple)
        {
            SelectOption option = new SelectOption(f.getValue(), f.getLabel());
            results.add(option);
        }
        return results;
    }
}