public class skedOption {
	public string id {get;set;}
    public string label {get;set;}
    public string value {get;set;}
    public string key {get;set;}

    public skedOption(string i, string v){
        id          = i;
        label       = v;
    }

    public skedOption(string i, string l,string k){
        id          = i;
        label       = l;
        key         = k;
    }

    public skedOption(){
        this.id         = '';
        this.label      = '';
        this.key      = '';
    }
}