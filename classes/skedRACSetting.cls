global class skedRACSetting {
    
    public integer firstDay {get;set;}
    public integer viewPeriod {get;set;}
    public string dateFormat {get;set;}
    public string datePickerFormat {get;set;}
    public integer noOfRecurWeeks {get;set;}
    public integer calendarStart {get;set;}
    public integer calendarEnd {get;set;}
    public integer calendarStep {get;set;}
    public boolean autoApproveAvailability {get;set;}
    public boolean enableActivity {get;set;}
    public boolean enableAvailabilityTemplate {get;set;}
    public boolean respectResourceTimezone {get;set;}
    
    public skedRACSetting() {
        sked_RAC_Setting__c setting = sked_RAC_Setting__c.getOrgDefaults();
        this.firstDay = 0;
        this.viewPeriod = 2;
        this.dateFormat = 'MM/dd/yyyy';
        this.datePickerFormat = 'D, mm/dd/yy';
        this.noOfRecurWeeks = 10;
        this.calendarStart = 700;
        this.calendarEnd = 1900;
        this.calendarStep = 30;
        
        if (setting.First_Day__c != NULL) {
            this.firstDay = integer.valueOf(setting.First_Day__c);
        }
        if (setting.View_Period__c != NULL) {
            this.viewPeriod = integer.valueOf(setting.View_Period__c);
        }
        if (!string.isBlank(setting.Date_Format__c)) {
            this.dateFormat = setting.Date_Format__c;
        }
        if (!string.isBlank(setting.Date_Picker_Format__c)) {
            this.datePickerFormat = setting.Date_Picker_Format__c;
        }
        if (setting.No_of_Recur_Weeks__c != NULL) {
            this.noOfRecurWeeks = integer.valueOf(setting.No_of_Recur_Weeks__c);
        }
        if (setting.Calendar_Start__c != NULL) {
            this.calendarStart = integer.valueOf(setting.Calendar_Start__c);
        }
        if (setting.Calendar_End__c != NULL) {
            this.calendarEnd = integer.valueOf(setting.Calendar_End__c);
        }
        if (setting.Calendar_Step__c != NULL) {
            this.calendarStep = integer.valueOf(setting.Calendar_Step__c);
        }
        this.autoApproveAvailability = setting.Auto_Approve_Availability__c;
        this.enableActivity = setting.Enable_Activity__c;
        this.enableAvailabilityTemplate = setting.Enable_Availability_Template__c;
        this.respectResourceTimezone = setting.Respect_Resource_Timezone__c;
    }
    
}