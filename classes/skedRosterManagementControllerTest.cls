@isTest
private class skedRosterManagementControllerTest {

  	@isTest 
    static void testSkedRosterManagementController() {

        skedTestDataFactory.setupCustomSettings();

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));

        Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        insert location;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;

        list<sked__Job__c> jobs = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2);
        for(sked__Job__c job : jobs){
            job.sked__Contact__c = client.Id;
        }
        jobs.get(0).sked__Duration__c = 23*60;
        jobs.get(0).sked__Finish__c = jobs.get(0).sked__Start__c.addMinutes(23*60);
        insert jobs; 
        
        String l = new skedRosterManagementController().locationOrSiteFilter;
        
        skedRosterManagementController.getConfigData();
        skedRosterManagementController.getShiftsByRegion(region.Id, System.now().format(skedRosterManagementController.DATE_FORMAT), skedCommonModels.PRIOD_WEEK, null, client.id, '');
        skedCommonModels.getClients('Test');

        
        skedRosterManagementController.rescheduleJob(jobs.get(0).Id, System.now().addDays(1).format(skedRosterManagementController.DATE_FORMAT), 1000);
        skedRosterManagementController.cancelJob(jobs.get(0).Id, 'No Reason', false);

        //Allocate resource
        sked__Resource__c resource = skedTestDataFactory.createResource('Test', UserInfo.getUserId(), region.Id);
        insert resource;

        skedTestDataFactory.allocateJob(jobs.get(0).Id, resource.Id);

        skedRosterManagementController.dispatchJob(jobs.get(0).Id);

        skedRosterManagementController.unscheduleJob(jobs.get(0).Id);

        Test.stopTest();
    }
}