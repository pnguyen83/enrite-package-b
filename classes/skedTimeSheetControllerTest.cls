@isTest
private class skedTimeSheetControllerTest
{
	@isTest
	static void fetchSettingsTest()
	{
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		test.startTest();
		skedTimesheetController.fetchSettings();
		test.stopTest();
	}

	@isTest
	static void fetchDateSlotsTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		test.startTest();
		skedTimesheetController.fetchDateSlots(resource.Id, String.valueOf(System.today()), '', '');
		test.stopTest();
	}

	@isTest
	static void fetchDateSlotsTest2() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		test.startTest();
		skedTimesheetController.fetchTimesheet(resource.Id, String.valueOf(System.today()));
		test.stopTest();
	}

	@isTest
	static void updateUnAvailabilityTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Availability__c unAvaiRec = (sked__Availability__c)mapData.get('availability 3');
		skedTimesheetModels.Availability unAvai = new skedTimesheetModels.Availability(unAvaiRec, unAvaiRec.sked__Timezone__c);
		test.startTest();
		skedTimesheetController.updateUnAvailability(unAvai);
		test.stopTest();	
	}

	@isTest
	static void submitTimesheetTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		skedTimesheetModels.ActionResult result = (skedTimesheetModels.ActionResult)skedTimesheetController.fetchDateSlots(resource.Id, String.valueOf(System.today()), '', '');
		List<skedTimesheetModels.Timesheet> lstTimesheet = (List<skedTimesheetModels.Timesheet>)result.data;
		sked__Job_Allocation__c jobAll = (sked__Job_Allocation__c)mapData.get('job allocation 1');
		sked__Activity__c activity = (sked__Activity__c)mapData.get('activity 1');

		skedTimesheetModels.Timesheet timesheet = lstTimesheet.get(0);
		timesheet.startDate = System.today();
		timesheet.endDate = System.today().addDays(7);
		skedTimesheetModels.Slots lstSlot = new skedTimesheetModels.Slots();
		skedTimesheetModels.Slots slot = new skedTimesheetModels.Slots();
        slot.label = jobAll.sked__Resource__r.name;
        //slot.dateString = tSheetDate;
        slot.id = jobAll.sked__Job__c;
        slot.name = jobAll.sked__Job__r.Name;
        slot.slotType = SkeduloConstants.JOB;
        //slot.slotColor = color.JOB_COLOR;
        //slot.start = new TimeModel(jobStart, timezone);
        //slot.finish = new TimeModel(jobFinish, timezone);
        slot.jobStatus = jobAll.sked__Job__r.sked__Job_Status__c;
        slot.travelTime = jobAll.sked__Job__r.Travel_Time__c; 
        slot.travelDistance = jobAll.Actual_Distance_Traveled_KM__c;
        slot.duration = (Integer)jobAll.sked__Job__r.sked__Duration__c;
        lstSlot.slots.add(slot);

        DateTime aStart = activity.sked__Start__c;
        DateTime aFinish = activity.sked__End__c;
        
        skedTimesheetModels.Slots slot1 = new skedTimesheetModels.Slots ();
        slot1.label = activity.sked__Type__c;
        //slot1.dateString = tSheetDate;
        slot1.duration = skedDateTimeUtils.getDifferenteMinutes(aStart, aFinish);
        slot1.id = activity.Id;
        slot1.slotType = SkeduloConstants.ACTIVITY;
        //slot1.slotColor = color.ACTIVITY_COLOR;
        //slot1.start = new TimeModel(aStart, timezone);
        //slot1.finish = new TimeModel(aFinish, timezone);
        
        lstSlot.slots.add(slot1);
        timesheet.slots.add(lstSlot);
		String payload = JSON.serialize(timesheet);
		System.debug('payload = ' + payload);
		test.startTest();
		skedTimesheetController.submitTimesheet(payload);
		test.stopTest();
	}

	@isTest static void submitTimesheetForApprovalTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		Timesheet__c timesheet = (Timesheet__c)mapData.get('timesheet 1');
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		test.startTest();
		skedTimesheetController.submitTimesheetForApproval(timesheet.id, resource.id);
		test.stopTest();
	}

	@isTest static void submitTimesheetForApprovalTest2() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		Timesheet__c timesheet = (Timesheet__c)mapData.get('timesheet 1');
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		resource.sked__User__c = null;
		update resource;
		test.startTest();
		skedTimesheetController.submitTimesheetForApproval(timesheet.id, resource.id);
		test.stopTest();
	}

	@isTest static void approveTimesheetTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		Timesheet__c timesheet = (Timesheet__c)mapData.get('timesheet 1');
		test.startTest();
		skedTimesheetController.approveTimesheet(timesheet.id);
		test.stopTest();
	}

	@isTest static void rejectTimesheetTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		Timesheet__c timesheet = (Timesheet__c)mapData.get('timesheet 1');
		test.startTest();
		skedTimesheetController.rejectTimesheet(timesheet.id,'abc');
		test.stopTest();
	}

	@isTest static void createActivityTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		skedTimesheetModels.Activity activity = new skedTimesheetModels.Activity();
		activity.resourceId = resource.id;
		activity.activityTypeId = 'BR';
		activity.startDate = String.valueOf(System.today());
		activity.startTime = 800;
		activity.finishTime = 1700;
		test.startTest();
		skedTimesheetController.createActivity(activity);
		test.stopTest();
	}

	@isTest static void updateActivityTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Activity__c act = (sked__Activity__c)mapData.get('activity 1');
		skedTimesheetModels.Activity activity = new skedTimesheetModels.Activity();
		activity.resourceId = act.sked__Resource__c;
		activity.activityTypeId = 'BR';
		activity.startDate = String.valueOf(System.today());
		activity.startTime = 800;
		activity.finishTime = 1700;
		activity.activityId = act.id;
		test.startTest();
		skedTimesheetController.updateActivity(activity);
		test.stopTest();
	}

	@isTest static void deleteActivityTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Activity__c act = (sked__Activity__c)mapData.get('activity 1');
		
		test.startTest();
		skedTimesheetController.deleteActivity(act.id);
		test.stopTest();
	}

	@isTest static void updateAvailabilityTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Availability__c avai = (sked__Availability__c)mapData.get('availability 1');
		skedTimesheetModels.AvailabilityModel avaiModel = new skedTimesheetModels.AvailabilityModel();
		avaiModel.id = avai.id;
		avaiModel.resourceId = avai.sked__Resource__c;
		avaiModel.startDate = avai.sked__Start__c.Date();
		avaiModel.finishDate = avai.sked__Finish__c.Date();
		avaiModel.startTime = 900;
		avaiModel.finishTime = 1200;
		String payload = JSON.serialize(avaiModel);
		test.startTest();
		skedTimesheetController.updateAvailability(payload);
		test.stopTest();
	}

	@isTest static void updateCompleteJobTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		sked__Job__c job = (sked__Job__c)mapData.get('job 1');
		sked__Job_Allocation__c allocation = (sked__Job_Allocation__c)mapData.get('job allocation 1');
		skedTimesheetModels.Job jobModel = new skedTimesheetModels.Job();
		jobModel.travelDistance = 50;
		jobModel.resourceId = allocation.sked__Resource__c;
		jobModel.startDate = job.sked__Start__c.Date();
		jobModel.finishDate = job.sked__Finish__c.Date();
		jobModel.actualStartDate = job.sked__Start__c.Date();
		jobModel.actualFinishDate = job.sked__Finish__c.Date();
		jobModel.jobId = job.id;
		jobModel.startTime = 800;
		jobModel.finishTime = 9000;
		jobModel.actualStartTime = 800;
		jobModel.actualFinishTime = 1000;
		jobModel.jobDuration = 60;
		String payload = JSON.serialize(jobModel);
		test.startTest();
		skedTimesheetController.updateCompleteJob(payload);
		test.stopTest();
	}

	@isTest static void skedTimesheetApprovalControllerTest() {
		Map<String, sObject> mapData = skedTimesheetTestUtil.createData();
		Timesheet__c ts = (Timesheet__c)mapData.get('timesheet 1');
		skedTimesheetApprovalController controller = new skedTimesheetApprovalController();
		test.startTest();
		controller.setTimesheetId(ts.id);
		controller.getTimesheetId();
		
		test.stopTest();
	}
}