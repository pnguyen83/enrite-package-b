public class skedTimesheetApprovalController {
    public string timesheetId;
    public string UserName {get;set;}
    public string ManagerName {get;set;}
    public string ResourceName {get;set;}
    public string StartDate {get;set;}
    public string EndDate {get;set;}
	
    public string getTimesheetId() {
        return this.timesheetId;
    }

    public void setTimesheetId(string inputId) {
        timesheetId = inputId;
        loadTimesheet();
    }
    
    private void loadTimesheet() {
        Timesheet__c ts = [SELECT Id, Resource__r.Name, Start_Date__c, End_Date__c, CreatedBy.Name
                           FROM Timesheet__c 
                           WHERE Id = :timesheetId];
        ResourceName = ts.Resource__r.Name;
        StartDate = ts.Start_Date__c.format();
        EndDate = ts.End_Date__c.format();
        UserName = ts.CreatedBy.Name;
        
        Id managerId = [Select Id, ManagerId FROM User WHERE Id = :UserInfo.getUserId()].ManagerId;
        ManagerName = [Select Id, Name FROM User WHERE Id = :managerId].Name;
    }
}