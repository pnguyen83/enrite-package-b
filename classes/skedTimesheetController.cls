public class skedTimesheetController extends skedTimesheetModels{
	
	/**
     * Fetch configuration data
     */
     @RemoteAction
    public static ActionResult fetchSettings() {
        return skedTimesheetUtils.getSetting();
    }

	/**
     * [fetchDateSlots fetch resource availability, unavailability, jobs, activities by date]
     * @param  {string} resourceId
     * @param  {string} dateString  'YYYY-MM-DD'
     * @param  {string} viewById    see settings
     * @param  {string} startingDay see settings
     * @return {[object]} collection of dateSlots
     * Slot Type Colors:
     * * Unavailability: #9aff9a
     * * Availability: #ffffff
     * * Activity: #ffd2a1
     * * Job: #e5c8f7
     */
	@RemoteAction
	public static ActionResult fetchDateSlots(String resourceId, String dateString, String viewById, String startingDayId){
		//String data = '[{"dateString":"2016-10-26","label":"Wed,Oct26","resourceId":"UID_1","availability":{"dateString":"2016-10-26","slotType":"Availability","slotColor":"#ffffff","start":{"id":900,"label":"09:00AM"},"finish":{"id":2030,"label":"08:30PM"},"duration":690},"unavailability":[{"dateString":"2016-10-26","slotType":"Unavailability","slotColor":"#9aff9a","start":{"id":0,"label":"00:00AM"},"finish":{"id":900,"label":"09:00AM"},"duration":540},{"dateString":"2016-10-26","slotType":"Unavailability","slotColor":"#9aff9a","start":{"id":2030,"label":"08:30PM"},"finish":{"id":2400,"label":"24:00AM"},"duration":210}],"slots":[{"label":"BillyMadison","dateString":"2016-10-26","id":"a1EO0000001RbiYMAS","name":"JOB-0001","slotType":"Job","slotColor":"#e5c8f7","start":{"id":1000,"label":"10:00AM"},"duration":60,"finish":{"id":1100,"label":"11:00PM"},"jobStatus":"InProgress","travelTime":30,"travelDistance":2},{"label":"BillyMadison","dateString":"2016-10-26","id":"a1EO0000001RbiYMAS","name":"JOB-0002","slotType":"Job","slotColor":"#e5c8f7","start":{"id":1130,"label":"11:30AM"},"duration":60,"finish":{"id":1230,"label":"12:30PM"},"jobStatus":"Complete","travelTime":15,"travelDistance":1.5},{"label":"Break","activityType":{"id":"BR","label":"Break"},"dateString":"2016-10-26","duration":285,"finish":{"id":1715,"label":"05:15PM"},"id":"a1EO0000001RmvuMAC","slotType":"Activity","slotColor":"#ffd2a1","start":{"id":1230,"label":"12:30PM"}},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":1515,"label":"03:15PM"},"id":"a1EO0000001RibZMAS","slotType":"Job","slotColor":"#F0F3F4","start":{"id":1415,"label":"02:15PM"},"jobStatus":"InProgress","travelTime":0},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":2100,"label":"09:00PM"},"id":"a1EO0000001RmWNMA0","slotType":"Job","slotColor":"#F0F3F4","start":{"id":2000,"label":"08:00PM"},"jobStatus":"InProgress","travelTime":0},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":2100,"label":"09:00PM"},"id":"a1EO0000001RmWNMA1","slotType":"Job","slotColor":"#F0F3F4","start":{"id":2000,"label":"08:00PM"},"jobStatus":"InProgress","travelTime":0}]},{"dateString":"2016-10-26","label":"Wed,Oct26","resourceId":"UID_1","availability":{"dateString":"2016-10-26","slotType":"Availability","slotColor":"#ffffff","start":{"id":900,"label":"09:00AM"},"finish":{"id":2030,"label":"08:30PM"},"duration":690},"unavailability":[{"dateString":"2016-10-26","slotType":"Unavailability","slotColor":"#9aff9a","start":{"id":0,"label":"00:00AM"},"finish":{"id":900,"label":"09:00AM"},"duration":540},{"dateString":"2016-10-26","slotType":"Unavailability","slotColor":"#9aff9a","start":{"id":2030,"label":"08:30PM"},"finish":{"id":2400,"label":"24:00AM"},"duration":210}],"slots":[{"label":"BillyMadison","dateString":"2016-10-26","id":"a1EO0000001RbiYMAS","name":"JOB-0001","slotType":"Job","slotColor":"#e5c8f7","start":{"id":1000,"label":"10:00AM"},"duration":60,"finish":{"id":1100,"label":"11:00PM"},"jobStatus":"InProgress","travelTime":30,"travelDistance":2},{"label":"BillyMadison","dateString":"2016-10-26","id":"a1EO0000001RbiYMAS","name":"JOB-0002","slotType":"Job","slotColor":"#e5c8f7","start":{"id":1130,"label":"11:30AM"},"duration":60,"finish":{"id":1230,"label":"12:30PM"},"jobStatus":"Complete","travelTime":15,"travelDistance":1.5},{"label":"Break","activityType":{"id":"BR","label":"Break"},"dateString":"2016-10-26","duration":285,"finish":{"id":1715,"label":"05:15PM"},"id":"a1EO0000001RmvuMAC","slotType":"Activity","slotColor":"#ffd2a1","start":{"id":1230,"label":"12:30PM"}},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":1515,"label":"03:15PM"},"id":"a1EO0000001RibZMAS","slotType":"Job","slotColor":"#F0F3F4","start":{"id":1415,"label":"02:15PM"},"jobStatus":"InProgress","travelTime":0},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":2100,"label":"09:00PM"},"id":"a1EO0000001RmWNMA0","slotType":"Job","slotColor":"#F0F3F4","start":{"id":2000,"label":"08:00PM"},"jobStatus":"InProgress","travelTime":0},{"label":"BillyMadison","dateString":"2016-10-26","duration":60,"finish":{"id":2100,"label":"09:00PM"},"id":"a1EO0000001RmWNMA1","slotType":"Job","slotColor":"#F0F3F4","start":{"id":2000,"label":"08:00PM"},"jobStatus":"InProgress","travelTime":0}]}]';
		//TODO
		//skedTimesheetUtils.getTimesheets();
		return skedTimesheetUtils.fetchDateSlots(resourceId, dateString, viewById, startingDayId);
	}

	/**
     * [fetchTimesheet fetch a timesheet record with a date in [startDate...endDate]]
     * @param  {string} resourceId
     * @param  {string} dateString  'YYYY-MM-DD'
     * @return {Timesheet} record or data: { id: null }
     */
     @RemoteAction
    public static Object fetchTimesheet(String resourceId, String dateString) {
    	//TODO
    	return skedTimesheetUtils.fetchTimesheet(resourceId, dateString);
    }

    @RemoteAction
    public static ActionResult updateUnAvailability(Availability unAvai) {
        return skedTimesheetUtils.updateUnAvailability(unAvai);
    }

    /**
     * 
     */
     @RemoteAction
    public static Object submitTimesheet(String payload) {
        SavePoint sp = Database.setSavepoint();
        ActionResult result = new ActionResult(true, '');
        try {
            skedTimesheetModels.Timesheet model = (skedTimesheetModels.Timesheet)JSON.deserialize(payload, skedTimesheetModels.Timesheet.class);
           
            Timesheet__c newTimeSheet = new Timesheet__c(
                Resource__c = model.resourceId,
                Start_Date__c = model.startDate,
                End_Date__c = model.endDate,
                Status__c = skedConstants.TIMESHEET_STATUS_DRAFT
            ); 
            insert newTimeSheet;
            
            Set<string> jobIds = new Set<string>();
            Set<string> activityIds = new Set<string>();
            List<sked__Job__c> skedJobs = new List<sked__Job__c>();
            List<sked__Activity__c> skedActivities = new List<sked__Activity__c>();

            for(skedTimesheetModels.Slots slots : model.slots) {
                for(skedTimesheetModels.Slots slot : slots.slots) {
                    System.debug('slot = ' + slot);
                    if(slot.slotType == SkeduloConstants.JOB) {
                        jobIds.add(slot.id);
                    }

                    if(slot.slotType == SkeduloConstants.ACTIVITY) {
                        activityIds.add(slot.id);
                    }
                }              
            }

            Map<string, sked__Job__c> mapJobs = new Map<string, sked__Job__c>([SELECT Id, Timesheet__c
                                                                               FROM sked__Job__c 
                                                                               WHERE Id IN :jobIds]);
            Map<string, sked__Activity__c> mapActivities = new Map<string, sked__Activity__c>([SELECT Id, Timesheet__c
                                                                                               FROM sked__Activity__c 
                                                                                               WHERE Id IN :activityIds]);
            for(skedTimesheetModels.Slots slots : model.slots) {
                for(skedTimesheetModels.Slots slot : slots.slots) {
                    if(slot.slotType == SkeduloConstants.JOB) {
                        sked__Job__c job = mapJobs.get(slot.id);
                        job.Timesheet__c = newTimeSheet.Id;
                        skedJobs.add(job);
                    }

                    if(slot.slotType == SkeduloConstants.ACTIVITY) {
                        sked__Activity__c activity = mapActivities.get(slot.id);
                        activity.Timesheet__c = newTimeSheet.Id;
                        skedActivities.add(activity);
                    }
                }              
            }
            update skedJobs;
            update skedActivities;
            
            result.data = newTimeSheet;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new ActionResult(false, ex.getMessage());
            System.debug('Error: ' + ex.getMessage());
            System.debug('stack trace = ' + ex.getStackTraceString());
        }      

    	return result;
    }

    @RemoteAction
    public static ActionResult submitTimesheetForApproval(String timesheetId, String resourceId) {
        ActionResult result = new ActionResult(true, '');

        if(!skedTimesheetHandler.submitTimesheetApproval(timesheetId, resourceId)) {
            result.success = false; 
            result.errorMessage = 'MANAGER_NOT_DEFINED';            
        }
        return result;
    }

    @RemoteAction
    public static ActionResult approveTimesheet(String timesheetId) {
        ActionResult result = new ActionResult(false, '');
        result.success = skedTimesheetHandler.approveTimesheet(timesheetId);
        return result;
    }

    @RemoteAction
    public static ActionResult rejectTimesheet(String timesheetId, String reason) {
        ActionResult result = new ActionResult(false, '');
        result.success = skedTimesheetHandler.rejectTimesheet(timesheetId, reason);
        return result;
    }

    /**
     * 
     */
     @RemoteAction
    public static Object createActivity(Activity model) {
    	//TODO
        SavePoint sp = Database.setSavepoint();
        ActionResult result = new ActionResult(true, '');
        try {    
            Date dateValue = (Date)Json.deserialize('"' + model.startDate + '"', Date.class);
            DateTime startOfDay = DateTime.newInstance(dateValue, Time.newInstance(0, 0, 0, 0));
            sked__Resource__c resource =    [SELECT Id, 
                                            sked__Primary_Region__c, 
                                            sked__Primary_Region__r.sked__TimeZone__c, 
                                            sked__Primary_Region__r.Name
                                            FROM sked__Resource__c
                                            WHERE Id = :model.resourceId];
            String timeZoneIdSid =  resource.sked__Primary_Region__r.sked__TimeZone__c;                                
            startOfDay = skedTimezoneUtil.ConvertBetweenTimezones(startOfDay, timeZoneIdSid, UserInfo.getTimeZone().getID());
            Integer startHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.startTime);
            Integer endHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.finishTime);
            DateTime startTime = skedTimezoneUtil.addMinutes(startOfDay, startHourInMinutes, timeZoneIdSid);
            DateTime endTime = skedTimezoneUtil.addMinutes(startOfDay, endHourInMinutes, timeZoneIdSid);

            sked_Activity_type__c actType = [SELECT Id__c, Label__c FROM sked_Activity_type__c WHERE Id__c = :model.activityTypeId];
            sked__Activity__c activity = new sked__Activity__c(
                                                                sked__Resource__c = model.resourceId,
                                                                sked__Start__c = startTime,
                                                                sked__End__c = endTime,
                                                                sked__Type__c = actType.Label__c,
                                                                sked__Address__c = resource.sked__Primary_Region__r.Name
                                                                );
            insert activity;
            result.data = activity.Id;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new ActionResult(false, ex.getMessage());
        }      
        
        return result;
    }

    /**
     * 
     */
     @RemoteAction
    public static Object updateActivity(Activity model) {
    	//TODO
        SavePoint sp = Database.setSavepoint();
        ActionResult result = new ActionResult(true, '');
        try {                        
            Date dateValue = skedTimezoneUtil.ConvertToDateValue(model.startDate);
            DateTime startOfDay = DateTime.newInstance(dateValue, Time.newInstance(0, 0, 0, 0));
            sked__Resource__c resource =    [SELECT Id, 
                                            sked__Primary_Region__c, 
                                            sked__Primary_Region__r.sked__TimeZone__c, 
                                            sked__Primary_Region__r.Name
                                            FROM sked__Resource__c
                                            WHERE Id = :model.resourceId];
            String timeZoneIdSid =  resource.sked__Primary_Region__r.sked__TimeZone__c;                                
            startOfDay = skedTimezoneUtil.ConvertBetweenTimezones(startOfDay, timeZoneIdSid, UserInfo.getTimeZone().getID());
            Integer startHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.startTime);
            Integer endHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.finishTime);
            DateTime startTime = skedTimezoneUtil.addMinutes(startOfDay, startHourInMinutes, timeZoneIdSid);
            DateTime endTime = skedTimezoneUtil.addMinutes(startOfDay, endHourInMinutes, timeZoneIdSid);

            sked_Activity_type__c actType = [SELECT Id__c, Label__c FROM sked_Activity_type__c WHERE Id__c = :model.activityTypeId];
            sked__Activity__c activity = new sked__Activity__c(
                                                                Id = model.activityId,
                                                                sked__Resource__c = model.resourceId,
                                                                sked__Start__c = startTime,
                                                                sked__End__c = endTime,
                                                                sked__Type__c = actType.Label__c,
                                                                sked__Address__c = resource.sked__Primary_Region__r.Name
                                                                );
            update activity;
            result.data = activity.Id;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new ActionResult(false, ex.getMessage());
        }      
        
        return result;
    }

    /**
     * 
     */
     @RemoteAction
    public static Object deleteActivity(String activityId) {
    	//TODO
    	SavePoint sp = Database.setSavepoint();
        ActionResult result = new ActionResult(true, '');
        try {            
            sked__Activity__c activity = new sked__Activity__c( Id = activityId );
            delete activity;
            result.data = activityId;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new ActionResult(false, ex.getMessage());
        }      
        
        return result;
    }

    /**
     * 
     */
     @RemoteAction
    public static Object updateAvailability(String payload) {
    	//TODO
        SavePoint sp = Database.setSavepoint();
        ActionResult result = new ActionResult(true, '');
        try {            
            skedTimesheetModels.AvailabilityModel model = (skedTimesheetModels.AvailabilityModel)JSON.deserialize(payload, skedTimesheetModels.AvailabilityModel.class);
            DateTime startTime = DateTime.newInstance(model.startDate, Time.newInstance(0, 0, 0, 0));
            DateTime finishTime = DateTime.newInstance(model.finishDate, Time.newInstance(0, 0, 0, 0));

            sked__Resource__c resource =    [SELECT Id, sked__Primary_Region__c, sked__Primary_Region__r.sked__TimeZone__c, 
                                                    sked__Primary_Region__r.Name
                                             FROM sked__Resource__c
                                             WHERE Id = :model.resourceId];
            String timeZoneIdSid =  resource.sked__Primary_Region__r.sked__TimeZone__c;            
            Integer startHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.startTime);
            Integer endHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.finishTime);
            startTime = skedTimezoneUtil.addMinutes(startTime, startHourInMinutes, timeZoneIdSid);
            finishTime = skedTimezoneUtil.addMinutes(finishTime, endHourInMinutes, timeZoneIdSid);

            sked__Availability__c availability = [SELECT Id, sked__Start__c, sked__Finish__c
                                                  FROM sked__Availability__c
                                                  WHERE Id = :model.id];
            availability.sked__Start__c = startTime;
            availability.sked__Finish__c = finishTime;
            update availability;
            result.data = availability;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new ActionResult(false, ex.getMessage());
        }      
        
        return result;
    }

    /**
     * 
     */
     @RemoteAction
    public static Object updateCompleteJob(String payload) {
        SavePoint sp = Database.setSavepoint();
        skedTimesheetModels.ActionResult result = new skedTimesheetModels.ActionResult(true, '');
        try {   
            boolean isActual = sked_Timesheet_Settings__c.getOrgDefaults().Display_Actual_Job_Time__c;          
            skedTimesheetModels.Job model = (skedTimesheetModels.Job)JSON.deserialize(payload, skedTimesheetModels.Job.class);
            System.debug('model.travelDistance = ' + model.travelDistance);
            System.debug('model.resourceId = ' + model.resourceId);
            DateTime startTime = DateTime.newInstance(model.startDate, Time.newInstance(0, 0, 0, 0));
            DateTime finishTime = DateTime.newInstance(model.finishDate, Time.newInstance(0, 0, 0, 0));
            DateTime actualStartTime = model.actualStartDate != null ? DateTime.newInstance(model.actualStartDate, Time.newInstance(0, 0, 0, 0)) : null;
            DateTime actualFinishTime = model.actualFinishDate != null ? DateTime.newInstance(model.actualFinishDate, Time.newInstance(0, 0, 0, 0)) : null;
            List<sObject> lstAllocations = new List<sObject>();

            sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Finish__c, sked__Duration__c, sked__TimeZone__c
                                    FROM sked__Job__c
                                    WHERE Id = :model.jobId];
            String timeZoneIdSid = skedJob.sked__TimeZone__c;
            Integer startHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.startTime);
            Integer endHourInMinutes = skedTimezoneUtil.ConvertTimeNumberToMinutes(model.finishTime);
            Integer actualStartHourInMinutes = model.actualStartTime != null ? skedTimezoneUtil.ConvertTimeNumberToMinutes(model.actualStartTime) : null; 
            Integer actualFinishHourInMinutes = model.actualFinishTime != null ? skedTimezoneUtil.ConvertTimeNumberToMinutes(model.actualFinishTime) : null;
            startTime = skedTimezoneUtil.addMinutes(startTime, startHourInMinutes, timeZoneIdSid);
            finishTime = skedTimezoneUtil.addMinutes(finishTime, endHourInMinutes, timeZoneIdSid); 
            actualStartTime = skedTimezoneUtil.addMinutes(actualStartTime, actualStartHourInMinutes, timeZoneIdSid);   
            actualFinishTime = skedTimezoneUtil.addMinutes(actualFinishTime, actualFinishHourInMinutes, timeZoneIdSid);           
            
            if (isActual == false) {
                skedJob.sked__Start__c = startTime;
                skedJob.sked__Finish__c = finishTime;
                skedJob.sked__Duration__c = model.jobDuration;    
            }
            

            if (actualStartTime != null) {
                skedJob.sked__Actual_Start__c = actualStartTime;    
            }
            
            if (actualFinishTime != null) {
                skedJob.sked__Actual_End__c = actualFinishTime;    
            }
            
            skedJob.Distance_Travel__c = model.travelDistance != null ? model.travelDistance : 0;
            //skedJob.Travel_Time__c = model.travelTime != null ? model.travelTime : 0;
            if (model.resourceId != null) {
                for (sked__Job_Allocation__c allocation : [SELECT Id 
                                                            FROM sked__Job_Allocation__c 
                                                            WHERE sked__Job__c = : model.jobId 
                                                            AND sked__Resource__c = :model.resourceId
                                                            AND sked__Job__r.sked__Job_Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
                                                            AND sked__Job__r.sked__Job_Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED
                                                            AND sked__Job__r.sked__Job_Status__c != :skedConstants.JOB_STATUS_CANCELLED]) {
                    if (model.travelDistance != null) {
                        allocation.Actual_Distance_Traveled_KM__c = model.travelDistance; 
                        lstAllocations.add(allocation);   
                    }
                }
            }
            lstAllocations.add(skedJob);
            system.debug(skedJob);
            update lstAllocations;
            result.data = skedJob;
        } catch(Exception ex) {
            Database.rollback(sp);
            return new skedTimesheetModels.ActionResult(false, ex.getMessage());
        }      
        
        return result;
    }
}