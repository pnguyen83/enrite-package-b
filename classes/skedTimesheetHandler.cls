public class skedTimesheetHandler {
	public static boolean submitTimesheetApproval(string timesheetId, String resourceId) {
        try {
            Timesheet__c ts = [SELECT Id, Resource__c, Approver__c, CreatedById FROM Timesheet__c WHERE Id = :timesheetId];
            sked__Resource__c resource = [SELECT Id, sked__User__c FROM sked__Resource__c WHERE Id = :resourceId];
            if(resource != NULL) {
                User usr =  [Select Id, ManagerId FROM User WHERE Id = :resource.sked__User__c];
                
                if(usr.ManagerId != null) {
                    ts.Approver__c = usr.ManagerId;
                    update ts;
                    
                    Approval.ProcessSubmitRequest approvalReq = new Approval.ProcessSubmitRequest();
                    approvalReq.setComments('Submitting request for Timesheet approval.');
                    approvalReq.setObjectId(ts.Id);
                    Approval.ProcessResult result = Approval.process(approvalReq);
                    return true;
                } 
            }
        }
        catch(Exception ex) {
            system.debug(ex.getMessage());
            return false;
        }
		return false;
	}

	public static boolean approveTimesheet(string timesheetId) {
		return processApproval(timesheetId, true, null);
	}

	public static boolean rejectTimesheet(string timesheetId, String reason) {
		return processApproval(timesheetId, false, reason);
	}

	private static boolean processApproval(string timesheetId, boolean isApproved, String reason) {	
		try {
			List<Id> newWorkItemIds = new List<Id>();
	        for (List<ProcessInstance> pis : [Select (Select Id From Workitems) From ProcessInstance p WHERE p.TargetObjectId = :timesheetId AND p.Status = 'Pending']) {
	            for (ProcessInstance pi : pis) {
	                for (List<ProcessInstanceWorkitem> wis : pi.Workitems) {
	                    for (ProcessInstanceWorkitem wi : wis ) {
	                        newWorkItemIds.add(wi.Id);
	                    }
	                }           
	            }
	        }
	        // Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
	        if (isApproved) {
	            req.setComments('Approving request.');
	            req.setAction('Approve');
	        } else {
	        	if (reason != null) {
	        		req.setComments('Rejecting request. Reason: ' + reason);	
	        	} else {
	        		req.setComments('Rejecting request.');
	        	}
	            
	            req.setAction('Reject');
	        }
	        
	        // Use the ID from the newly created item to specify the item to be worked
	        req.setWorkitemId(newWorkItemIds.get(0));
	        
	        // Submit the request for approval
	        Approval.ProcessResult result =  Approval.process(req);

	        return true;
		}	
		catch(Exception ex) {
			return false;
		}		
	}
}