public virtual class skedTimesheetModels {
	
	public class Timesheet {
		public String id;
		public string approvalStatus;
		public String dateString;
		public String label;
		public String resourceId;
		public String weekDay;
		public Date startDate;
		public Date endDate;
		//public Availability availability;
		public List<Availability> availability;
		public List<Availability> unavailability;
		public List<Slots> slots;

		public Timesheet () {
			this.availability = new List<Availability>();
			this.unavailability = new List<Availability>();
			this.slots = new List<Slots>();
		}

		public TimeSheet (TimeSheet__c tSheet) {
			this.id = tSheet.id;
			this.approvalStatus = tSheet.Status__c;
			this.resourceId = tSheet.Resource__c;
			this.startDate = tSheet.Start_Date__c;
			this.endDate = tSheet.End_Date__c;
		}
	}

	public class Availability {
		public String dateString;
		public String slotType;
		public String slotColor;
		public TimeModel start;
		public TimeModel finish;
		public Integer duration;
		public String id;
		public String resourceId;
		public DateTime startTime;
		public DateTime finishTime;
		public String recordType;
		public Boolean isAvailable;

		public Availability() {

		}

		public Availability(sked__Availability__c a, String timezone){
			DateTime avaiStart = a.sked__Start__c;
			DateTime avaiFinish = a.sked__Finish__c;
			this.startTime = avaiStart;
			this.finishTime = avaiFinish;
			this.start 	= new TimeModel(avaiStart, timezone);
            this.finish 	= new TimeModel(avaiFinish, timezone);
            this.id 			= a.Id != null ? a.Id : '';
            this.recordType = a.sked__Type__c != null ? a.sked__Type__c : '';
            this.slotType		= a.sked__Is_Available__c==true?SkeduloConstants.AVAILABILITY:SkeduloConstants.UNAVAILABILITY;
            this.dateString 	= avaiStart.format('yyyy-MM-dd', timezone);
            this.isAvailable = a.sked__Is_Available__c;
            this.duration = skedDateTimeUtils.getDifferenteMinutes(avaiStart, avaiFinish);
            this.resourceId = a.sked__Resource__c != null ? a.sked__Resource__c : '';
		}
	}

	public class AvailabilityModel {
		public String id;
		public String resourceId;
		public Date startDate;
		public Date finishDate;
		public Integer startTime;
		public Integer finishTime;
	}

	public class Slots {
		public String label;
		public String dateString;
		public String id;
		public String name;
		public String slotType;
		public String slotColor;
		public TimeModel start;
		public Integer duration;
		public TimeModel finish;
		public String jobStatus;
		public decimal travelTime;
		public decimal travelDistance;
		public Item activityType;
		public String availabilityId;
		public List<Slots> slots;
		public TimeModel actualStart;
		public TimeModel actualFinish;

		public Slots() {
			slots = new List<Slots>();
		}
	}

	public class TimeModel {
        public Integer id;
        public String label;
        public Integer value;
        
        public TimeModel(DateTime dt, String timezone){
            String d = dt==null?'':dt.format('Hmm', timezone);//Hour in day (0-23)
            this.value = Integer.valueOf(d);
            if(d==''){
                this.id = 0;
            }else{
                this.id = Integer.valueOf(d);
            }
            
            this.label = dt==null?'':dt.format('hh:mm a', timezone);//Hour in am/pm (1-12)
        }
    }

	public class Item {
        public String id;
        public String label;
        public Item(String id, String label){
            this.id = id;
            this.label = label;
        }
    }

	public class ActionResult{
		public boolean success;        
        public Object data;

        public String errorMessage {
        	get;
            set{
                errorMessage = value;
				if( errorMessage.contains('INACTIVE_OWNER_OR_USER,') ) {
                    errorMessage = errorMessage.substringBetween('INACTIVE_OWNER_OR_USER,', ':');
                }
            }
        }

        public ActionResult(boolean success, String msg){
            this.success   = success;
            this.errormessage        = msg;
            this.data     	= '';
        }
	}

	public class SettingModel {
		public List<StartingDayOptions> startingDayOptions;
		public List<ViewByOptions> viewByOptions;
		public List<Times> times;
		public List<ActivityTypes> activityTypes;
		public List<Item> availabilityTypes;
		public List<Resources> resources;
		public Messages	CONFIRM_APPROVAL;
		public Messages CONFIRM_REJECTION;
		public Messages MANAGER_NOT_DEFINED;

		public SettingModel () {
			this.startingDayOptions = new List<StartingDayOptions>();
			this.viewByOptions = new List<ViewByOptions>();
			this.times = new List<Times>();
			this.activityTypes = new List<ActivityTypes>();
			this.resources = new List<Resources>();
			this.availabilityTypes = new List<Item>();
		}
	}

	public class Times {
		public String label;
		public Decimal id;

		public Times (Decimal id, String label) {
			this.id = id;
			this.label = label;
		}
	}

	public class StartingDayOptions {
		public String id;
		public String label;
		public Boolean isDefault;
		public Decimal value;

		public StartingDayOptions (sked_Starting_Date_Options__c dayOption) {
			this.id = String.isNotBlank(dayOption.Id__c) ? dayOption.Id__c : '';
			this.label = String.isNotBlank(dayOption.Label__c) ? dayOption.Label__c : '';
			this.isDefault = dayOption.isDefault__c;
			this.value = dayOption.Value__c != null ? dayOption.Value__c : 0;
		}
	}

	public class ViewByOptions {
		public String id;
		public String label;
		public Boolean isDefault;
		public String unit;
		public Decimal amount;

		public ViewByOptions (sked_View_Options__c viewOption) {
			this.id = String.isNotBlank(viewOption.Id__c) ? viewOption.Id__c : '';
			this.label = String.isNotBlank(viewOption.Label__c) ? viewOption.Label__c : '';
			this.isDefault = viewOption.isDefault__c;
			this.unit = String.isNotBlank(viewOption.Unit__c) ? viewOption.Unit__c : '';
			this.amount = viewOption.Amount__c != null ? viewOption.Amount__c : 0;
		}
	}

	public class ActivityTypes {
		public String id;
		public String label;
		public Boolean isDefault;

		public ActivityTypes (sked_Activity_type__c actType) {
			this.id = String.isNotBlank(actType.Id__c) ? actType.Id__c : '';
			this.label = String.isNotBlank(actType.Label__c) ? actType.Label__c : '';
			this.isDefault = actType.isDefault__c;
		}
	}

	public class Resources {
		public String id;
		public String label;
		public Boolean isLoggedIn;
		public Boolean isDefault;

		public Resources (sked__resource__c res) {
			this.id = res.Id;
			this.label = res.name;
			String currentUserId = UserInfo.getUserId();
			if (res.sked__User__c.equals(currentUserId)) {
				this.isLoggedIn = true;
				this.isDefault = true;
			} else {
				this.isLoggedIn = false;
				this.isDefault = false;
			}
		}
	}

	public class Messages {
		public String CONFIRM_SUBMISSION;

		public Messages (String mes) {
			this.CONFIRM_SUBMISSION = mes;
		}
	}

	public class RosterModel {
        public Datetime startDate {get; set;}
        public Datetime finishDate {get; set;}
        
        public RosterModel() {
            
        }
    }

    public class Activity {
    	public String activityId;
    	public String resourceId;
    	public String activityTypeId;
    	public Integer startTime;
    	public Integer finishTime;
    	public String finishDate;
    	public String startDate;
    }

    public class Job {
    	public String jobId;
    	public DateTime start;
    	public DateTime finish;
    	public Date startDate;
    	public Date finishDate;
    	public Integer startTime;
    	public Integer finishTime;
    	public Integer jobDuration;
    	public Decimal travelTime;
    	public Decimal travelDistance;
    	public DateTime actualStart;
    	public DateTime actualFinish;
    	public Integer actualStartTime;
    	public Integer actualFinishTime;
    	public Date actualStartDate;
    	public Date actualFinishDate;
    	public String resourceId;
    }
}