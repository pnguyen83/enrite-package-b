public class skedTimesheetTriggerHandler {
	public static void onAfterInsert(List<TimeSheet__c> lstNew) {
		updateTimesheetRelated(lstNew);
	}

	public static void onAfterUpdate(List<TimeSheet__c> lstNew) {
		updateTimesheetRelated(lstNew);
	}

	public static void updateTimesheetRelated(List<TimeSheet__c> lstNew) {
		List<sObject> lstObject = new List<sObject>();
		Map<String, List<wrapper>> mapResAvais = new Map<String, List<wrapper>>();
		Map<String, List<wrapper>> mapResActivities = new Map<String, List<wrapper>>();
		Set<String> setResIds = new Set<String>();
		Date startDate;
		Date endDate;

		for (Timesheet__c ts : lstNew) {
			if (startDate == null || startDate > ts.Start_Date__c) {
				startDate = ts.Start_Date__c;
			}
			if (endDate == null || endDate < ts.End_Date__c) {
				endDate = ts.End_Date__c;
			}
			setResIds.add(ts.Resource__c);
		}

		DateTime startTime = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0));
		DateTime endTime = DateTime.newInstance(endDate, Time.newInstance(0, 0, 0, 0));
		endTime = endTime.addDays(1).addSeconds(-1);

		//get list availability of the timesheet
		for (sked__Availability__c avai : [SELECT Id, sked__Start__c, sked__Finish__c, sked__Resource__c 
											FROM sked__Availability__c 
											WHERE sked__Resource__c IN : setResIds
											AND ((sked__Start__c >= :startTime AND sked__Start__c <= :endTime) 
					                            OR (sked__Finish__c >= :startTime AND sked__Finish__c <= :endTime)
					                        	)
					                        AND Timesheet__c = null
										]) {
			List<wrapper> lstAvais = mapResAvais.get(avai.sked__Resource__c);
			if (lstAvais == null) {
				lstAvais = new List<wrapper>();
			}								
			lstAvais.add(new wrapper(avai));
			mapResAvais.put(avai.sked__Resource__c, lstAvais);
		}

		for (sked__Activity__c activity : [SELECT ID, sked__Start__c, sked__End__c, sked__Resource__c 
							FROM sked__Activity__c 
							WHERE sked__Resource__c IN : setResIds
							AND ((sked__Start__c >= :startDate AND sked__Start__c <= :endDate) 
	                            OR (sked__End__c >= :startDate AND sked__End__c <= :endDate)
	                        	)
	                        AND Timesheet__c = null
						]) {
			List<wrapper> lstActivities = mapResActivities.get(activity.sked__Resource__c);
			if (lstActivities == null) {
				lstActivities = new List<wrapper>();
			}
			lstActivities.add(new wrapper(activity));
			mapResActivities.put(activity.sked__Resource__c, lstActivities);
		}

		for (TimeSheet__c ts : lstNew) {
			if (mapResAvais.containsKey(ts.Resource__c)) {
				List<wrapper> lstAvais = mapResAvais.get(ts.Resource__c);
				for (wrapper avai : lstAvais) {
					if (avai.startDate >= ts.Start_Date__c && avai.startDate <= ts.End_Date__c ||
						avai.endDate >= ts.Start_Date__c && avai.endDate <= ts.End_Date__c
						) {
						sked__Availability__c updateAvai = new sked__Availability__c(
							Timesheet__c = ts.Id,
							id = avai.id
						);	
						lstObject.add(updateAvai);
					}
				}
			}

			if (mapResActivities.containsKey(ts.Resource__c)) {
				List<wrapper> lstActs = mapResActivities.get(ts.Resource__c);
				for (wrapper avai : lstActs) {
					if (avai.startDate >= ts.Start_Date__c && avai.startDate <= ts.End_Date__c ||
						avai.endDate >= ts.Start_Date__c && avai.endDate <= ts.End_Date__c
						) {
						sked__Activity__c updateAvai = new sked__Activity__c(
							Timesheet__c = ts.Id,
							id = avai.id
						);	
						lstObject.add(updateAvai);
					}
				}
			}
		}

		if (!lstObject.isEmpty()) {
			lstObject.sort();
			SavePoint sp = Database.setSavePoint();
			try {
				update lstObject;
			} catch(Exception ex) {
				Database.rollback(sp);
			}
		}
	}

	public class wrapper {
		public string id;
		public Date startDate;
		public Date endDate;
		
		public wrapper (sked__Availability__c avai) {
			this.id = avai.id;
			this.startDate = avai.sked__Start__c.Date();
			this.endDate = avai.sked__Finish__c.Date();	
		}	

		public wrapper (sked__Activity__c act) {
			this.id = act.id;
			this.startDate = act.sked__Start__c.Date();
			this.endDate = act.sked__end__c.Date();
		}
	}
}