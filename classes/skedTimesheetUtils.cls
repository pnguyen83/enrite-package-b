public class skedTimesheetUtils extends skedTimesheetModels{
	public static String DATE_FORMAT = 'MM/dd/yyyy';

    /*
    *
    */
/*
    public Map<string, Timesheet> InitilizeDateSlots(Date fromDate, Date toDate, String timezone) {
            Map<string, Timesheet> timesheetMap = new Map<string, Timesheet>();
            Set<Date> allHolidays = new Set<Date>();
            
            Date tmpFromDate = fromDate;
            while (tmpFromDate <= toDate) {
                
                DateTime dateTimeTemp = DateTime.newInstance(tmpFromDate, Time.newInstance(12, 0, 0, 0));
                Timesheet ts = new Timesheet();
                ts.dateString = dateTimeTemp.format(DATE_FORMAT, timezone);
                timesheetMap.put(ts.dateString, ts);

                tmpFromDate = tmpFromDate.addDays(1);
            }
            return timesheetMap;
        }
*/
    //get all necessary information for timesheet grid
    //SEP-24
    public static ActionResult getSetting() {
        SettingModel setting = new SettingModel();
        ActionResult result = new ActionResult(false, '');
       
        //get View By options
        for (sked_View_Options__c viewOption : [SELECT Id, Id__c, Amount__c, isDefault__c, Label__c, Unit__c FROM sked_View_Options__c]) {
            ViewByOptions vModel = new ViewByOptions(viewOption);
            setting.viewByOptions.add(vModel);
        }

        //get starting date option
        for (sked_Starting_Date_Options__c startOption : [SELECT Id, Id__c, isDefault__c, Label__c, Value__c FROM sked_Starting_Date_Options__c]) {
            StartingDayOptions dayOption = new StartingDayOptions(startOption);
            setting.startingDayOptions.add(dayOption);
        }

        //get list of Times
        sked_Timesheet_Settings__c timeSetting = sked_Timesheet_Settings__c.getOrgDefaults();
        
        if (timeSetting == null) {
            result.errormessage = SkeduloConstants.NO_TIMESHEET_SETTING; 
            return result;
        } else {
            List<Times> lstTimes = initTimeSlotSettings(timeSetting.Start_Time__c, timeSetting.End_Time__c);
            setting.times = lstTimes;
        }

        //get activity types
        for (sked_Activity_type__c activityType : [SELECT Id, Id__c, isDefault__c, Label__c FROM sked_Activity_type__c]) {
            ActivityTypes actType = new ActivityTypes(activityType);
            setting.activityTypes.add(actType);
        }

        //get availability types
        Schema.DescribeFieldResult fieldResult = sked__Availability__c.sked__Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f : ple) {
            Item avaiType = new Item(f.getLabel(), f.getLabel());
            setting.availabilityTypes.add(avaiType);
        }

        //get active resources
        List<Resources> lstRes = getActiveResources();
        if (lstRes == null || lstRes.isEmpty()) {
            result.errormessage = SkeduloConstants.NO_ACTIVE_RESOURCE; 
            return result;
        } else {
            setting.resources.addAll(lstRes);
        }

        if (setting.viewByOptions.isEmpty()) {
            result.errormessage = SkeduloConstants.NO_VIEW_OPTION_SETTING; 
            return result;
        }

        if (setting.startingDayOptions.isEmpty()) {
            result.errormessage = SkeduloConstants.NO_STARTING_DATE_OPTION_SETTING; 
            return result;
        }

        if (setting.activityTypes.isEmpty()) {
            result.errormessage = SkeduloConstants.NO_ACTIVITY_TYPE_SETTING; 
            return result;
        }

        setting.CONFIRM_APPROVAL = new Messages(SkeduloConstants.TIMESHEET_COMFIRMATION_MESSAGE);
        setting.CONFIRM_REJECTION = new Messages(SkeduloConstants.TIMESHEET_REJECTION_MESSAGE);
        setting.MANAGER_NOT_DEFINED = new Messages(SkeduloConstants.MANAGER_NOT_DEFINED);

        result.data = setting;
        result.success = true;

        return result;
    }

    //init time slot value for Grid Setting information
    public static List<Times> initTimeSlotSettings(Decimal startTime, Decimal endTime) {
        List<Times> lstTimes = new List<Times>();
        Integer startInteger = Integer.valueOf(startTime);
        Integer endInteger = Integer.valueOf(endTime);

        for (Integer i = startInteger; i <= endInteger; i = i + 100) {
        //for (Integer i = 0; i <= 2400; i = i + 100) {
            String label = String.valueOf(i/100);

            if (i > 1200) {
                label = String.valueOf((i - 1200)/100);
            }
            
            if (i == 0) {
                label = '12am';
            } else if (i == 1200) {
                label += 'pm';
            }

            Times newTime = new Times(i, label);
            lstTimes.add(newTime);
        }

        return lstTimes;
    }

    //get active resources in system
    public static List<Resources> getActiveResources () {
        List<Resources> lstResources = new List<Resources>();
        boolean hasDefault = false;

        for (sked__Resource__c res : [SELECT Id, Name, sked__Is_Active__c, sked__User__c FROM sked__Resource__c WHERE sked__Is_Active__c = true AND sked__User__c != '']) {
            Resources resource = new Resources(res);
            if (resource.isDefault) {
                hasDefault = true;
            }

            lstResources.add(resource);
        }

        if (!hasDefault && !lstResources.isEmpty()) {
            Resources resource = lstResources.get(0);
            resource.isDefault = true;
        }

        return lstResources;
    }

    //fetch timesheet
    public static ActionResult fetchTimesheet (String resourceId, String dateString) {
        ActionResult result = new ActionResult(false, '');
        List<TimeSheet> lstTimesheet = new List<TimeSheet>();
        Date selectedDate = Date.valueOf(dateString);
        try {
            for (Timesheet__c tSheet : [SELECT Id, End_Date__c, Resource__c, Start_Date__c, Status__c 
                                    FROM Timesheet__c 
                                    WHERE Resource__c =:resourceId
                                    AND Start_Date__c <= :selectedDate AND End_Date__c >= :selectedDate
                                    //AND Status__c != : SkeduloConstants.APPROVAL_STATUS
                                    //AND Status__c != : SkeduloConstants.REJECT_STATUS
                                    ]) {
                TimeSheet tModel = new TimeSheet(tSheet);
                lstTimesheet.add(tModel);
            }

            result.success = true;
            result.data = lstTimesheet;
        } catch (Exception ex) {
            result.errorMessage = ex.getMessage();
        }

        return result;
    }

    //fetch dateslot for one resource on one date
    public static ActionResult fetchDateSlots (String resourceId, String dateString, String viewById, String startingDayId) {
        ActionResult result = new ActionResult(false, '');
        Date dateValue = Date.valueOf(dateString);
        Integer period = 0;
        RosterModel currentRoster;

        Map<String, List<Availability>> mapAvailabilities = new Map<String, List<Availability>>();
        Map<String, List<Availability>> mapUnAvailabilities = new Map<String, List<Availability>>();
        Map<String, List<sked__Job_Allocation__c>> mapDateJobAllocations = new Map<String, List<sked__Job_Allocation__c>>();
        Map<String, List<sked__Activity__c>> mapDateActivities = new Map<String, List<sked__Activity__c>>();
        Map<String, sked__Availability_Template_Entry__c> mapTemplateEntries = new Map<String, sked__Availability_Template_Entry__c>();
        Map<String, String> mapActivityTypes = new Map<String, String>();
        List<Timesheet> lstTimesheet = new List<Timesheet>();
        //String resourceTimeZone = UserInfo.getTimeZone().getID();
        List<sked__Resource__c> resources = [SELECT Id, sked__Primary_Region__r.sked__Timezone__c FROM sked__Resource__c WHERE Id =:resourceId];

        String resourceTimeZone = '';
        if ( resources.size() >0 ) {
            resourceTimeZone = resources[0].sked__Primary_Region__r.sked__Timezone__c;
        }
        else {
            result.errormessage = SkeduloConstants.TIMESHEET_NO_REGION;
            return result;
        }

        //get time sheet type from custom setting
        sked_Roster_Setting__c rosterSetting = sked_Roster_Setting__c.getOrgDefaults();
        if (rosterSetting == null) {
            result.errormessage = SkeduloConstants.ROSTER_SETTING_NOT_DEFINED;
            return result;
        }

        if (rosterSetting.Timesheet_Type__c.equalsIgnoreCase(SkeduloConstants.TIMESHEET_FORTNIGHT_TYPE)) {
            period = (Integer)rosterSetting.Period__c;
        } else if (rosterSetting.Timesheet_Type__c.equalsIgnoreCase(SkeduloConstants.TIMESHEET_WEEK_TYPE)) {
            period = 7;
        }    

        //get current roster
        currentRoster = getCurrentRoster(rosterSetting, period);
        
        if (currentRoster != null) {
            try {
                DateTime rosterStart = currentRoster.startDate;
                DateTime rosterFinish = currentRoster.finishDate;
                DateTime selectedDate = DateTime.newInstance(dateValue, Time.newInstance(0, 0, 0, 0));
                Integer startHourInMinutes = rosterStart.hour() * 60 + rosterStart.minute();
                
                selectedDate = skedDateTimeUtils.addMinutes(selectedDate, startHourInMinutes, resourceTimeZone);
                
                //loop to get the roster of selected date
                while (selectedDate < rosterStart || selectedDate > rosterFinish) {
                    if (selectedDate < rosterStart) {
                        rosterStart = skedDateTimeUtils.addDays(rosterStart, -period, resourceTimeZone);
                        rosterFinish = skedDateTimeUtils.addDays(rosterFinish, -period, resourceTimeZone);
                    } else if (selectedDate > rosterFinish) {
                        rosterStart = skedDateTimeUtils.addDays(rosterStart, period, resourceTimeZone);
                        rosterFinish = skedDateTimeUtils.addDays(rosterFinish, period, resourceTimeZone);
                    } else if (selectedDate <= rosterFinish && selectedDate >= rosterStart) {
                        break;
                    }
                }
                
                //get activity type from custom setting
                mapActivityTypes = getActivityTypeSetting();
                
                //init timesheet
                lstTimesheet = initTimeSheet(rosterStart, rosterFinish, resourceId, resourceTimeZone);
                
                //get availability and unavailability in roster
                getAvailabilities(rosterStart, rosterFinish, resourceId, mapAvailabilities, mapUnAvailabilities, resourceTimeZone, resourceId);
                
                //get jobs in roster
                mapDateJobAllocations = getJobsInRoster(rosterStart, rosterFinish, resourceId, resourceTimeZone);
                
                //get activities in roster
                mapDateActivities = getActivitiesInRoster(rosterStart, rosterFinish, resourceId, resourceTimeZone);

                //get template entries for resource in roster
                mapTemplateEntries = getTemplateEntries(resourceId, rosterStart, rosterFinish);
                
                //update slots based on availability template entry
                updateTimeSlotWithTemplateEntries(lstTimesheet, mapTemplateEntries, resourceId, resourceTimeZone);
                
                //update slots based on availability
                updateTimeSlotWithAvailabilities(lstTimesheet, mapAvailabilities, true);

                //update slots based on unAvailability
                updateTimeSlotWithAvailabilities(lstTimesheet, mapUnAvailabilities, false);

                //update slots based on jobs
                updateTimeSlotWithJobs(lstTimesheet, mapDateJobAllocations, resourceTimeZone);

                //update slots with activities
                updateTimeSlotWithActivities(lstTimesheet, mapDateActivities, resourceTimeZone, mapActivityTypes);

                result.success = true;
                result.data = lstTimesheet;

                
            }catch (exception ex) {
                result.errormessage = ex.getMessage();
            }
        } else {
            result.errormessage = SkeduloConstants.NO_RSOTER_INFORMATION;
        }
        
        return result;
    }

    //get available of a resource in a roster
    public static void getAvailabilities(DateTime startDate, DateTime endDate, String resourceID, Map<String, List<Availability>> mapAvailabilities, 
                                            Map<String, List<Availability>> mapUnAvailabilities, String timezone, String resoureID) {
        ColorModel color = new ColorModel();
        
        for (sked__Availability__c avai: [SELECT Id, sked__Start__c, sked__Finish__c, sked__Status__c, sked__Timezone__c, sked__Is_Available__c, sked__Type__c, sked__Resource__c
                                            FROM sked__Availability__c
                                            WHERE sked__Status__c = :SkeduloConstants.AVAILABILITY_APPROVED_STATUS
                                            AND sked__Start__c >= :startDate
                                            AND sked__Finish__c <= :endDate
                                            AND sked__Resource__c = :resoureID
                                            ]) {
            Availability avaiModel = new Availability(avai, timezone);
            
            List<Availability> lstAvais = mapAvailabilities.get(avaiModel.dateString);

            if (lstAvais == null) {
                lstAvais = new List<Availability>();
            }    
            lstAvais.add(avaiModel);

            if (avai.sked__Is_Available__c) {
                avaiModel.slotColor = color.AVAILABLE_COLOR;
                mapAvailabilities.put(avaiModel.dateString, lstAvais);
            } else {
                avaiModel.slotColor = color.UNAVAILABLE_COLOR;
                mapUnAvailabilities.put(avaiModel.dateString, lstAvais);
            }
        }
    }

    //update availability
    public static ActionResult updateUnAvailability(Availability unAvai) {
        ActionResult result = new ActionResult(false, '');

        List<sked__Resource__c> lstRes = [SELECT Id, sked__Primary_Region__c, sked__Primary_Region__r.sked__Timezone__c 
                                    FROM sked__Resource__c 
                                    WHERE Id = :unAvai.resourceID];
        if (lstRes == null) {
            result.errorMessage = SkeduloConstants.TIMESHEET_INVALID_RESOURCE_ID;
            return result;
        }

        String timezone = lstRes.get(0).sked__Primary_Region__r.sked__Timezone__c;
        Date onDate = Date.valueOf(unAvai.dateString);
        DateTime startDate = DateTime.newInstance(onDate, Time.newInstance(0, 0, 0, 0));
        startDate = skedDateTimeUtils.GetStartOfDate(startDate, timezone);
        Integer startMinutes = skedDateTimeUtils.convertTimeNumberToMinutes((Integer)unAvai.start.Id);
        Integer endMinutes = skedDateTimeUtils.convertTimeNumberToMinutes((Integer)unAvai.finish.Id);
        DateTime newStart = skedDateTimeUtils.addMinutes(startDate, startMinutes, timezone);
        DateTime newFinish = skedDateTimeUtils.addMinutes(startDate, endMinutes, timezone);
        sked__Availability__c updateAvai = new sked__Availability__c(
            id = unAvai.id,
            sked__Type__c = unAvai.recordType,
            sked__Start__c = newStart,
            sked__Finish__c = newFinish,
            sked__Is_Available__c = unAvai.isAvailable,
            sked__Resource__c = unAvai.resourceId
        );

        SavePoint sp = Database.setSavePoint();
        try {
            update updateAvai;
            result.success = true;
            result.data = new Availability(updateAvai, timezone);
        } catch (Exception ex) {

        }

        return result;
    }

    //get jobs of resource in roster
    public static Map<String, List<sked__Job_Allocation__c>> getJobsInRoster(DateTime startDate, DateTime endDate, String resourceID, String timezone) {
        Map<String, List<sked__Job_Allocation__c>> mapDateJobs = new Map<String, List<sked__Job_Allocation__c>>();

        for (sked__Job_Allocation__c jobAll : [SELECT Id, sked__Job__c, sked__Job__r.name, sked__Job__r.sked__Start__c, sked__Job__r.sked__Finish__c, sked__Job__r.sked__Timezone__c,
                                                sked__Job__r.sked__Job_Status__c, sked__Job__r.sked__Duration__c , sked__Resource__c, sked__Resource__r.name,
                                                Est_Distance_Traveled_KM__c, sked__Estimated_Travel_Time__c, sked__Job__r.Travel_Time__c, sked__Job__r.Distance_Travel__c,
                                                sked__Job__r.sked__Actual_End__c, sked__Job__r.sked__Actual_Start__c, Actual_Distance_Traveled_KM__c
                                                FROM sked__Job_Allocation__c 
                                                WHERE sked__Resource__c = :resourceID 
                                                AND sked__Job__r.sked__Start__c >= :startDate 
                                                AND sked__Job__r.sked__Finish__c <= :endDate
                                                AND sked__Job__r.sked__Job_Status__c != :skedConstants.JOB_STATUS_CANCELLED
                                                AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
                                                ]) {
            //DateTime jobStart = skedDateTimeUtils.convertTimezone(jobAll.sked__Job__r.sked__Start__c, 'UTC', timezone);
            DateTime jobStart = jobAll.sked__Job__r.sked__Start__c;
            String jobDate = jobStart.format('yyyy-MM-dd', timezone);
            List<sked__Job_Allocation__c> lstJobAlls = mapDateJobs.get(jobDate);
            if (lstJobAlls == null) {
                lstJobAlls = new List<sked__Job_Allocation__c>();
            }
            lstJobAlls.add(jobAll);
            mapDateJobs.put(jobDate, lstJobAlls);
        }
        
        return mapDateJobs;
    }

    //get activity of resource in roster
    public static Map<String, List<sked__Activity__c>> getActivitiesInRoster (DateTime startDate, DateTime endDate, String resourceID, String timezone) {
        Map<String, List<sked__Activity__c>> mapDateActivities = new Map<String, List<sked__Activity__c>>();
        System.debug('startDate = ' + startDate);
        System.debug('endDate = ' + endDate);
        for (sked__Activity__c activity : [SELECT Id, Name, sked__Timezone__c, sked__Type__c, sked__Resource__c, sked__Start__c, sked__End__c
                                            FROM sked__Activity__c 
                                            WHERE sked__Resource__c = :resourceID
                                            AND ((sked__Start__c >= :startDate AND sked__Start__c <= :endDate) 
                                                    OR (sked__end__c >= :startDate AND sked__end__c <= :endDate)
                                                )
                                            ]) {
            SYstem.debug('activity = ' + activity);
            //DateTime activityStart = skedDateTimeUtils.convertTimezone(activity.sked__Start__c, 'UTC', timezone); 
            DateTime activityStart = activity.sked__Start__c;
            String activityDate = activityStart.format('yyyy-MM-dd', timezone);
            List<sked__Activity__c> lstActivities = mapDateActivities.get(activityDate);
            if (lstActivities == null) {
                lstActivities = new List<sked__Activity__c>();
            }
            lstActivities.add(activity);
            mapDateActivities.put(activityDate, lstActivities);
        }

        return mapDateActivities;
    }

    //get availability template entry
    public static Map<String, sked__Availability_Template_Entry__c> getTemplateEntries(String resourceID, DateTime startDate, DateTime endDate) {
        Map<String, sked__Availability_Template_Entry__c> mapAvaiTemplEntry = new Map<String, sked__Availability_Template_Entry__c>();
        Set<String> setAvaiTemplates = new Set<String>();

        //get availability template resource IDs
        for (sked__Availability_Template_Resource__c resAvai : [SELECT Id, sked__Availability_Template__c 
                                                                FROM sked__Availability_Template_Resource__c 
                                                                WHERE sked__Resource__c = :resourceID]) {
            setAvaiTemplates.add(resAvai.sked__Availability_Template__c);
        }

        //get availability template entries
        for (sked__Availability_Template__c avaiTemplate : [SELECT Id, sked__Start__c, sked__Finish__c,
                                                            (SELECT Id, sked__Finish_Time__c, sked__Is_Available__c, sked__Start_Time__c, 
                                                                sked__Weekday__c FROM sked__Availability_Template_Entries__r WHERE sked__Is_Available__c = true) 
                                                            FROM sked__Availability_Template__c WHERE Id IN : setAvaiTemplates 
                                                            ]) {
            for (sked__Availability_Template_Entry__c entry : avaiTemplate.sked__Availability_Template_Entries__r) {
                String weekDate = entry.sked__Weekday__c.toUpperCase();
                sked__Availability_Template_Entry__c avaiEntry = mapAvaiTemplEntry.get(weekDate);
                if (avaiEntry != null) {
                    if ((avaiTemplate.sked__Start__c == null && avaiTemplate.sked__Finish__c == null) ||
                        (avaiTemplate.sked__Start__c >= startDate.date() && avaiTemplate.sked__Finish__c <= endDate.date()) ||
                        (startDate.date() <= avaiTemplate.sked__Finish__c && endDate.date() >= avaiTemplate.sked__Finish__c) || 
                        (startDate.date() <= avaiTemplate.sked__Start__c && avaiTemplate.sked__Start__c <= endDate.date()) ||
                        (startDate.date() >= avaiTemplate.sked__Start__c && endDate.date() <= avaiTemplate.sked__Finish__c)
                        ) {
                        
                        if (avaiTemplate.sked__Start__c != null && avaiTemplate.sked__Finish__c != null) {
                            System.debug('avaiTemplate.sked__Start__c = ' + avaiTemplate.sked__Start__c);
                            System.debug('avaiTemplate.sked__Finish__c = ' + avaiTemplate.sked__Finish__c);
                            System.debug('startDate = ' + startDate.date());
                            System.debug('endDate = ' + endDate.date());
                        }

                        if (avaiEntry.sked__Start_Time__c > entry.sked__Start_Time__c) {
                            avaiEntry.sked__Start_Time__c = entry.sked__Start_Time__c;
                        }
                        if (avaiEntry.sked__Finish_Time__c < entry.sked__Finish_Time__c) {
                            avaiEntry.sked__Finish_Time__c = entry.sked__Finish_Time__c;
                        }
                        
                        mapAvaiTemplEntry.put(weekDate, avaiEntry);
                    }
                    
                } else {
                    if (avaiTemplate.sked__Start__c != null && avaiTemplate.sked__Finish__c != null) {
                        System.debug('2 avaiTemplate.sked__Start__c = ' + avaiTemplate.sked__Start__c);
                        System.debug('2 avaiTemplate.sked__Finish__c = ' + avaiTemplate.sked__Finish__c);
                        System.debug('2 startDate = ' + startDate.date());
                        System.debug('2 endDate = ' + endDate.date());
                    }
                    if ((avaiTemplate.sked__Start__c == null && avaiTemplate.sked__Finish__c == null) ||
                        (avaiTemplate.sked__Start__c >= startDate.date() && avaiTemplate.sked__Finish__c <= endDate.date()) ||
                        (startDate.date() <= avaiTemplate.sked__Finish__c && endDate.date() >= avaiTemplate.sked__Finish__c) || 
                        (startDate.date() <= avaiTemplate.sked__Start__c && avaiTemplate.sked__Start__c <= endDate.date()) ||
                        (startDate.date() >= avaiTemplate.sked__Start__c && endDate.date() <= avaiTemplate.sked__Finish__c)
                        ) {
                            mapAvaiTemplEntry.put(weekDate, entry);
                        }
                }
                
            }
        }

        return mapAvaiTemplEntry;
    }


    //create empty timesheet
    public static List<Timesheet> initTimeSheet(DateTime startDate, DateTime endDate, String resourceId, String timezone) {
        List<Timesheet> lstTimeSheet = new List<Timesheet>();
        DateTime tempDate = startDate;
        
        while (tempDate < endDate) {
            Timesheet tSheet = new Timesheet();
            String weekDate = tempDate.format('EEE', timezone);
            tSheet.dateString = tempDate.format('yyyy-MM-dd', timezone);
            tSheet.label = weekDate + ', ' + getMonthName(tempDate.month()) + ' ' + tempDate.day();
            tSheet.resourceId = resourceId;
            tSheet.weekDay = weekDate;
            lstTimeSheet.add(tSheet);
            tempDate = tempDate.addDays(1);
        }

        return lstTimeSheet;
    }
/*
    //init timeslots
    public static void initTimeSlot (TimeSheet tSheet, Date timeSheetDate, Integer avaiStart, Integer avaiEnd, String timezone) {
        List<Slots> lstSlot = new List<Slots>();
        Integer temp = 0;
        Integer year = timeSheetDate.year();
        Integer month = timeSheetDate.month();
        Integer day = timeSheetDate.day();
        ColorModel color = new ColorModel();

        while (temp <= avaiEnd) {
            Slots slot = new Slots();
            DateTime slotStart = DateTime.newInstance(year, month, day, temp, 0, 0);
            
            slot.start = new TimeModel(slotStart, timezone);
            slot.duration = 60;
            slot.finish = new TimeModel(slotStart.addMinutes(60), timezone);

            if (temp < avaiStart || temp > avaiEnd) {
                slot.slotColor = color.UNAVAILABLE_COLOR;
            } else if (temp >= avaiStart && temp <= avaiEnd) {
                slot.slotColor = color.AVAILABLE_TEMPLATE_ENTRY_COLOR;
            } 

            lstSlot.add(slot);
            temp++;
        }

        tSheet.slots.addAll(lstSlot);
    }
*/
    //update timeslot value based on template entry
    public static void updateTimeSlotWithTemplateEntries (List<Timesheet> lstTimesheet, Map<String, sked__Availability_Template_Entry__c> mapAvaiTemplEntries, String resourceID, String timezone) {
        for (Timesheet tSheet : lstTimesheet) {
            String weekDay = tSheet.weekDay.toUpperCase();
            Date tSheetDay = Date.valueOf(tSheet.dateString);
            DateTime startOfDay = DateTime.newInstance(tSheetDay, Time.newInstance(0, 0, 0, 0));
            
            if (mapAvaiTemplEntries.containsKey(weekDay)) {
                sked__Availability_Template_Entry__c avai = mapAvaiTemplEntries.get(weekDay);
                if (avai != null) {
                    Integer minutesStart = skedDateTimeUtils.convertTimeNumberToMinutes((Integer)avai.sked__Start_Time__c);
                    Integer minutesFinish = skedDateTimeUtils.convertTimeNumberToMinutes((integer)avai.sked__Finish_Time__c);
                    DateTime startTime = startOfDay.addMinutes(minutesStart);
                    DateTime finishTime = startOfDay.addMinutes(minutesFinish);
                    //get unavailability from 0 AM to start time of availability template entry
                    tSheet.unavailability.addAll(createUnavailabilityFromAvaiEntry(startOfDay,startTime, tSheet.dateString, timezone, resourceID));
                    //get unavailability from end time of availability template entry to end of date
                    tSheet.unavailability.addAll(createUnavailabilityFromAvaiEntry(finishTime,skedDateTimeUtils.addMinutes(startOfDay, 24*60, timezone), tSheet.dateString, timezone, resourceID));
                } 
            } else {
                System.debug('weekDay = ' + weekDay);
                tSheet.unavailability.addAll(createUnavailabilityFromAvaiEntry(startOfDay,skedDateTimeUtils.addMinutes(startOfDay, 24*60, timezone), tSheet.dateString, timezone, resourceID));
            }
        }
    }

    //create unavailability for timesheet based on template entry
    public static List<Availability> createUnavailabilityFromAvaiEntry(DateTime start, DateTime finish, String dateString, String timezone, String resourceId) {
        ColorModel color = new ColorModel();
        List<Availability> lstUnavailability = new List<Availability>();
        DateTime tempStart = start;

        Availability avai = new Availability();
        avai.dateString = dateString;
        avai.slotType = SkeduloConstants.UNAVAILABILITY;
        avai.slotColor = color.UNAVAILABLE_COLOR;
        avai.start = new TimeModel(start, timezone);
        avai.finish = new TimeModel(finish, timezone);
        avai.duration = skedDateTimeUtils.getDifferenteMinutes(start, finish);
        avai.resourceId = resourceId;
        avai.startTime = start;
        avai.finishTime = finish;
        if (avai.finish.value == 0 && avai.finish.value < avai.start.value) {
            avai.finish.value = 2400;
            avai.finish.id = 2400;
        }
        lstUnavailability.add(avai);
        
        return lstUnavailability;
    }

    //update timeslot value based on job
    public static void updateTimeSlotWithJobs (List<Timesheet> lstTimesheet, Map<String, List<sked__Job_Allocation__c>> mapDateJobs, String timezone) {
        ColorModel color = new ColorModel();
        boolean isActual = sked_Timesheet_Settings__c.getOrgDefaults().Display_Actual_Job_Time__c;

        for (Timesheet tSheet : lstTimesheet) {
            String tSheetDate = tSheet.dateString;
            
            if (mapDateJobs.containsKey(tSheetDate)) {
                List<sked__Job_Allocation__c> lstJobAlls = mapDateJobs.get(tSheetDate);

                for (sked__Job_Allocation__c jobAll : lstJobAlls) {
                    DateTime jobStart = jobAll.sked__Job__r.sked__Start__c;
                    DateTime jobFinish = jobAll.sked__Job__r.sked__Finish__c;

                    //update for SEP-232
                    if (isActual && jobAll.sked__Job__r.sked__Job_Status__c == SkeduloConstants.JOB_STATUS_COMPLETE) {
                        jobStart = jobAll.sked__Job__r.sked__Actual_Start__c;
                        jobFinish = jobAll.sked__Job__r.sked__Actual_End__c;
                    }

                    Slots slot = new Slots();
                    slot.label = jobAll.sked__Resource__r.name;
                    slot.dateString = tSheetDate;
                    slot.id = jobAll.sked__Job__c;
                    slot.name = jobAll.sked__Job__r.Name;
                    slot.slotType = SkeduloConstants.JOB;
                    slot.slotColor = color.JOB_COLOR;
                    slot.start = new TimeModel(jobStart, timezone);
                    slot.finish = new TimeModel(jobFinish, timezone);
                    if (jobAll.sked__Job__r.sked__Actual_Start__c != null) {
                        slot.actualStart = new TimeModel(jobAll.sked__Job__r.sked__Actual_Start__c, timezone);
                    } else {
                        slot.actualStart = slot.start;
                    }
                    if (jobAll.sked__Job__r.sked__Actual_End__c != null) {
                        slot.actualFinish = new TimeModel(jobAll.sked__Job__r.sked__Actual_End__c, timezone);
                    } else {
                        slot.actualFinish = slot.finish;
                    }

                    //check for case job finish at 24 pm
                    if (slot.finish.value < slot.start.value && slot.finish.value == 0) {
                        slot.finish.value = 2400;
                        slot.finish.id = 2400;
                    }
                    slot.jobStatus = jobAll.sked__Job__r.sked__Job_Status__c;
                    slot.travelTime = jobAll.sked__Job__r.Travel_Time__c; 
                    slot.travelDistance = jobAll.Actual_Distance_Traveled_KM__c;
                    slot.duration = (Integer)jobAll.sked__Job__r.sked__Duration__c;
                    tSheet.slots.add(slot);
                }
            }
        }
    }

    //update timeslot value based on activities
    public static void updateTimeSlotWithActivities (List<Timesheet> lstTimesheet, Map<String, List<sked__Activity__c>> mapDateActivities, String timezone, Map<String, String> mapActivityTypes) {
        ColorModel color = new ColorModel(); 
        for (TimeSheet tSheet : lstTimesheet) {
            String tSheetDate = tSheet.dateString;

            if (mapDateActivities.containsKey(tSheetDate)) {
                List<sked__Activity__c> lstActivities = mapDateActivities.get(tSheetDate);

                for (sked__Activity__c activity : lstActivities) {
                    DateTime aStart = activity.sked__Start__c;
                    DateTime aFinish = activity.sked__End__c;
                    
                    Slots slot = new Slots ();
                    slot.label = activity.sked__Type__c;
                    slot.dateString = tSheetDate;
                    slot.duration = skedDateTimeUtils.getDifferenteMinutes(aStart, aFinish);
                    slot.id = activity.Id;
                    slot.slotType = SkeduloConstants.ACTIVITY;
                    slot.slotColor = color.ACTIVITY_COLOR;
                    slot.start = new TimeModel(aStart, timezone);
                    slot.finish = new TimeModel(aFinish, timezone);
                    if (slot.finish.value == 0 && slot.start.value > slot.finish.value) {
                        slot.finish.value = 2400;
                        slot.finish.id = 2400;
                    }
                    tSheet.slots.add(slot);
                    
                    if (mapActivityTypes.containsKey(activity.sked__Type__c)) {
                        slot.activityType = new Item(mapActivityTypes.get(activity.sked__Type__c), activity.sked__Type__c);
                    }
                    
                }
            }
        }
    }

    //update timeslot value based on availability and unavailability
    public static void updateTimeSlotWithAvailabilities (List<Timesheet> lstTimesheet, Map<String, List<Availability>> mapAvailabilities, boolean isAvailable) {
        ColorModel color = new ColorModel();    
        String slotColor = isAvailable == true ? color.AVAILABLE_COLOR : color.UNAVAILABLE_COLOR;
        String slotType = isAvailable == true ? SkeduloConstants.AVAILABILITY : SkeduloConstants.UNAVAILABILITY;
        for (Timesheet tSheet : lstTimesheet) {
            String tSheetDate = tSheet.dateString;

            if (mapAvailabilities.containsKey(tSheetDate)) {
                List<Availability> lstAvais = mapAvailabilities.get(tSheetDate);
                if (isAvailable) {
                    tSheet.availability.addAll(lstAvais);
                } else {
                    tSheet.unavailability.addAll(lstAvais);
                }
                
                for (Availability avai : lstAvais) {
                    Slots slot = new Slots();
                    slot.dateString = tSheetDate;
                    slot.slotType = slotType;
                    slot.slotColor = slotColor;
                    slot.start = avai.start;
                    slot.finish = avai.finish;
                    slot.duration = avai.duration;
                    slot.availabilityId = avai.Id;
                    if (slot.finish.value == 0 && slot.start.value > slot.finish.value) {
                        slot.finish.value = 2400;
                        slot.finish.id = 2400;
                    }
                    System.debug('slot.finish.id = ' + slot.finish.id + ', isAvailable = ' + isAvailable + ', slotColor = ' + slotColor);
                    tSheet.slots.add(slot);
                } 
            }
        }
    }

    //get name of month
    public static String getMonthName (Integer month) {
        if(month < 0 || month > 12) return '';
        return new String[]{'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'}[month - 1];
    }

    //get activity type in custom setting
    public static map<String, string> getActivityTypeSetting () {
        Map<String, String> mapActivityTypes = new Map<String, String>();
        for (sked_Activity_type__c activity : [SELECT Id, Id__c, isDefault__c, Label__c FROM sked_Activity_type__c]) {
            mapActivityTypes.put(activity.Label__c, activity.Id__c);
        }

        return mapActivityTypes;
    }

    //get current roster
    public static RosterModel getCurrentRoster(sked_Roster_Setting__c rosterSetting, Integer period) {
        RosterModel result = new RosterModel();
        
        if(rosterSetting != null) {
            DateTime rosterStartDate = rosterSetting.Start_Date__c;
            //rosterStartDate = skedDateTimeUtils.ConvertBetweenTimezones(rosterStartDate, 'UTC', UserInfo.getTimeZone().getID());
            //DateTime rosterEndDate = rosterStartDate.addDays((Integer)rosterSetting.Period__c);
            DateTime rosterEndDate = rosterStartDate.addDays(period);
            while(rosterEndDate < System.now()){
                //rosterEndDate = rosterEndDate.addDays((Integer)rosterSetting.Period__c);
                rosterEndDate = rosterEndDate.addDays(period);
            }
            
            //result.startDate = rosterEndDate.addDays(-(Integer)rosterSetting.Period__c);
            result.startDate = rosterEndDate.addDays(-period);
            result.finishDate = rosterEndDate.addMinutes(-1);
            
            return result;
            
        }
        return null;
    }

    public class ColorModel {
        public String AVAILABLE_COLOR;
        public String UNAVAILABLE_COLOR;
        public String JOB_COLOR;
        public String ACTIVITY_COLOR;
        //public String AVAILABLE_TEMPLATE_ENTRY_COLOR;

        public ColorModel() {
            sked_Timesheet_Settings__c setting = sked_Timesheet_Settings__c.getOrgDefaults();
            this.AVAILABLE_COLOR = setting.Availability_Color__c;
            this.UNAVAILABLE_COLOR = setting.Unavailability_Color__c;
            this.JOB_COLOR = setting.Job_Color__c;
            this.ACTIVITY_COLOR = setting.Activity_Color__c;
          //  this.AVAILABLE_TEMPLATE_ENTRY_COLOR = setting.Available_Template_Entry_Color__c;
        }
    }
}