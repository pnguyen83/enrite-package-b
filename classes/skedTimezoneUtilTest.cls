@isTest
private class skedTimezoneUtilTest
{
	static testmethod void doTest() {
		test.starttest();
		Integer offset = skedTimezoneUtil.getOffsetInMinute(system.now());
		System.assert(offset != null);
		Integer offset2 = skedTimezoneUtil.getOffsetInMinute('America/New_York', system.now());
		System.assert(offset2 != null);
		Integer offset3 = skedTimezoneUtil.getOffsetInMinute('America/New_York', 'America/Los_Angeles', Datetime.now());
		System.assert(offset3 != null);
		Integer result = skedTimezoneUtil.convertTimezone('America/New_York', 'America/Los_Angeles', Datetime.now(), 920);
		System.assert(result != null);
		DateTime timeResult = skedTimezoneUtil.convertToGMT('Asia/Ho_Chi_Minh', '2016-04-21 08:00:00');
		System.assert(timeResult != null);
		DateTime timeResult2 = skedTimezoneUtil.convertToGMT('America/New_York', system.now());
		System.assert(timeResult2 != null);
		DateTime timeResult3 = skedTimezoneUtil.convertFromGMT(system.now(), 'America/New_York');
		System.assert(timeResult3 != null);
		DateTime timeResult4 = skedTimezoneUtil.convertTimezone(system.now(), 'Asia/Ho_Chi_Minh', 'America/New_York');
		System.assert(timeResult4 != null);
		Integer minutes = skedTimezoneUtil.toMinute(800);
		System.assert(minutes != null);
		Integer times = skedTimezoneUtil.toTime(800);
		System.assert(times != null);
		String timeString = skedTimezoneUtil.toTimeString(800);
		System.assert(timeString != null);
		Integer timeResult5 = skedTimezoneUtil.convertTimeString2Integer(timeString);
		test.stoptest();
	}
	/*
	static testmethod void skedJobServiceQueryControllerTest() {
		test.starttest();
		ApexPages.currentPage().getParameters().put('id','test id');
		ApexPages.currentPage().getParameters().put('method','test method');
		skedJobServiceQueryController controller = new skedJobServiceQueryController();
		String s = controller.getResult();
		test.stoptest();
	}
	*/
}