@isTest
public class sked_SMC_Test {
    
    static testmethod void MainTest() {
        DateTime currentTime = system.now();
        Map<string, sObject> mapTestData = skedDataSetup.setupSMCTestData();
        
        sked__Resource__c resourceTeamLeader = (sked__Resource__c)mapTestData.get('resourceTeamLeader');
        sked__Region__c regionQld = (sked__Region__c)mapTestData.get('regionQld');
        
        sked_SMC_Controller.saveShiftModel saveShiftModel = getNotRecurringShiftModel(resourceTeamLeader, regionQld);
        sked_SMC_Controller.saveShiftModel reucrringShiftModel = getRecurringShiftModel(resourceTeamLeader, regionQld);
        
        sked_SMC_Controller.saveReplicationModel replicateModel = getReplicateModel(resourceTeamLeader, regionQld);
        
        Test.startTest();
        
        sked_SMC_Controller.getConfigData();
        sked_SMC_Controller.getPickListValues('sked__Availability__c', 'sked__Type__c');
        
        sked_SMC_Controller.getProviderList(currentTime.format('yyyy-MM-dd'));
        
        sked_SMC_Controller.saveShiftCreation(Json.serialize(saveShiftModel));
        sked_SMC_Controller.saveShiftCreation(Json.serialize(reucrringShiftModel));
        
        sked_SMC_Controller.saveReplication(Json.serialize(replicateModel));
        
        sked_SMC_Controller.saveShiftModel modelForUpdateDelete = getShiftModel();
        sked_SMC_Controller.saveShiftCreation(Json.serialize(modelForUpdateDelete));
        sked_SMC_Controller.deleteShift(Json.serialize(modelForUpdateDelete));
        
        Test.stopTest();
    }
    
    static sked_SMC_Controller.saveShiftModel getNotRecurringShiftModel(sked__Resource__c resource, sked__Region__c region) {
        DateTime currentTime = system.now();
        sked_SMC_Controller.saveShiftModel saveShiftModel = new sked_SMC_Controller.saveShiftModel();
        saveShiftModel.id = '';
        saveShiftModel.resourceId = resource.Id;
        saveShiftModel.regionTimezone = region.sked__Timezone__c;
        saveShiftModel.shiftType = 'Available';
        saveShiftModel.dateString = currentTime.format('dd/MM/yyyy');
        saveShiftModel.startTime = 700;
        saveShiftModel.endTime = 1500;
        saveShiftModel.recurringShift = false;
        saveShiftModel.skipWeekends = false;
        saveShiftModel.canOverride = false;
        
        return saveShiftModel;
    }
    
    static sked_SMC_Controller.saveShiftModel getRecurringShiftModel(sked__Resource__c resource, sked__Region__c region) {
        DateTime currentTime = system.now();
        sked_SMC_Controller.saveShiftModel reucrringShiftModel = new sked_SMC_Controller.saveShiftModel();
        reucrringShiftModel.id = '';
        reucrringShiftModel.resourceId = resource.Id;
        reucrringShiftModel.regionTimezone = region.sked__Timezone__c;
        reucrringShiftModel.shiftType = 'Available';
        reucrringShiftModel.dateString = currentTime.format('dd/MM/yyyy');
        reucrringShiftModel.startTime = 700;
        reucrringShiftModel.endTime = 1500;
        reucrringShiftModel.recurringShift = true;
        reucrringShiftModel.numOfDays = 7;
        reucrringShiftModel.skipWeekends = false;
        reucrringShiftModel.canOverride = false;
        
        return reucrringShiftModel;
    }
    
    static sked_SMC_Controller.saveReplicationModel getReplicateModel(sked__Resource__c resource, sked__Region__c region) {
        DateTime currentTime = system.now();
        sked_SMC_Controller.saveReplicationModel model = new sked_SMC_Controller.saveReplicationModel();
        model.resourceId = resource.Id;
        model.regionTimezone = region.sked__Timezone__c;
        model.repeatOption = 'Weekly';
        model.canOverride = true;
        model.sourceDate = currentTime.format('dd/MM/yyyy');
        model.startDateString = currentTime.addDays(7).format('dd/MM/yyyy');
        model.endDateString = currentTime.addDays(14).format('dd/MM/yyyy');
        
        return model;
    }
    
    static sked_SMC_Controller.saveShiftModel getShiftModel() {
        sked__Availability__c availability = [SELECT Id, sked__Resource__c, sked__Timezone__c
                                              FROM sked__Availability__c LIMIT 1];
        DateTime currentTime = system.now();
        sked_SMC_Controller.saveShiftModel saveShiftModel = new sked_SMC_Controller.saveShiftModel();
        saveShiftModel.id = availability.Id;
        saveShiftModel.resourceId = availability.sked__Resource__c;
        saveShiftModel.regionTimezone = availability.sked__Timezone__c;
        saveShiftModel.shiftType = 'Available';
        saveShiftModel.dateString = currentTime.format('dd/MM/yyyy');
        saveShiftModel.startTime = 700;
        saveShiftModel.endTime = 1500;
        saveShiftModel.recurringShift = false;
        saveShiftModel.skipWeekends = false;
        saveShiftModel.canOverride = false;
        
        return saveShiftModel;
    }
    
}