trigger skedAlertTrigger on enrtcr__Alert__c (after insert, after update, after delete) 
{
    Set<Id> contactIds = new Set<Id>();

    if (Trigger.isDelete)
    {
        for (enrtcr__Alert__c a : Trigger.Old)
        {
            contactIds.add(a.enrtcr__Client__c);
        }
    }
    else
    {
        for (enrtcr__Alert__c a : Trigger.New)
        {
            contactIds.add(a.enrtcr__Client__c);
        }
    }

    skedTriggerHandler handler = new skedTriggerHandler();
    handler.updateJobsForClientAlerts(contactIds);
}