trigger skedEventTrigger on Event (after insert, after update, after delete) {
	skedTriggerUtil.isFromEvent = true;

	if( Trigger.isAfter){
		if(Trigger.isUpdate){
			for(Event e: trigger.new){
				if(!String.isBlank(e.Job_Allocation_ID__c) && !skedTriggerUtil.isFromJob && !skedTriggerUtil.isFromJobAllocation){
					e.addError('This event is Locked');
				}
			}

		}

		if(Trigger.isDelete){
			for(Event e: trigger.old){
				if(!String.isBlank(e.Job_Allocation_ID__c) && !skedTriggerUtil.isFromJob && !skedTriggerUtil.isFromJobAllocation){
					e.addError('This event is Locked');
				}
			}
			//Delete related Activities
			skedTriggerUtil.deleteActivities( Trigger.old );
		}

		if(Trigger.isInsert || Trigger.isUpdate){
			if(skedTriggerUtil.isFromSkeduloActivity || skedTriggerUtil.isFromJobAllocation)  return;
			skedTriggerUtil.syncEventsToActivities( Trigger.new);
		}
	}
	
}