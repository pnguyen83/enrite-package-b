trigger skedJobServiceItemTrigger on Job_Service_Item__c (after insert,before insert,after update,before delete) {
    skedTriggerHandler handler = new skedTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //RW-43 populate value of Related Id
            for(Job_Service_Item__c jsi : Trigger.new){
                if(jsi.Service_Agreement_Item__c!=null){
                    jsi.Related_Id__c = String.valueOf(jsi.Service_Agreement_Item__c);
                }
            }
        }
    }
    if(trigger.isAfter && trigger.isInsert){
        //Create Service Delivered when Job Service Item is created : RW-60
        handler.createServiceDelivery(trigger.new); 
    }
    
    if(trigger.isAfter && trigger.isUpdate){
        //Update Related Service Delivered Record : RW-60
        handler.updateServiceDelivery(trigger.new,trigger.oldMap);
        //update Actual_Distance_Traveled_KM__c on Job Allocation from Job Service Item : SEP-25
        handler.updateActualDistanceTraveled(trigger.new, trigger.old);
    }
    
    if(trigger.isBefore && trigger.isDelete){
        //Delete Related Service Delivery when Job Service Item is Deleted
        handler.deleteServiceDelivered(trigger.old);
    }
}