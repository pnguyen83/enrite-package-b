trigger skedJobTriggers on sked__Job__c (after insert,after update, before Delete,before insert,before update) {
    skedTriggerUtil.isFromJob = true;
    skedTriggerHandler handler = new skedTriggerHandler();

    if(Trigger.isAfter){
        if(trigger.isInsert){
            //Copy Group attenddees to Created Job
            skedJobTriggerHandler.generateGroupAttendee(trigger.new);
            // create Job Tags
            handler.createJobTagsFromClientTags(trigger.new);
            //create Job Service Item
            handler.createJobServiceItem(trigger.new);            
            //update Job Field - Service Agreement based on service agreement item - RW-30
            handler.initFieldUpdatesOnCreate(trigger.new);
            //update Historical Case Notes on Job Complete
            handler.updateHistoricalCaseNotesOnInsert(trigger.newMap);
            //update Client Alerts on Job Complete
            //handler.updateClientAlertsOnInsert(trigger.new);
            //Update Geolocation
            skedLocationServices.updateLocationGeocode(Trigger.old, Trigger.newMap, skedLocationServices.OBJ_JOB);
        }
    }

    if(Trigger.isUpdate && trigger.isAfter){
        handler.updateJobServiceItem(trigger.new, trigger.oldMap);
        //generate Note to Contact
        skedJobTriggerHandler.generateContactNote(trigger.new, trigger.oldMap);
        //Update related events if job start/end time is changed
        list<Id> jobIDs = skedTriggerUtil.getJobIDs_StartEndTimeChanged(Trigger.new, Trigger.old, Trigger.oldMap );
        map<id, sked__Job_Allocation__c> relatedJAMap = new map<id,sked__Job_Allocation__c>([select Id from sked__Job_Allocation__c where sked__Job__c in:jobIDs]);
        skedTriggerUtil.syncJobAllocationsToEvents( new list<Id>( relatedJAMap.keySet() ) );
        //Delete related events if job's status is cancelled
        list<Id> cancelledJobIDs = skedTriggerUtil.getJobIDs_StatusChanged(new Set<String>{ 'Cancelled' }, Trigger.new, Trigger.old, Trigger.oldMap );
        list<sked__Job_Allocation__c> jaList = [select Id from sked__Job_Allocation__c where sked__Job__c in:cancelledJobIDs];
        skedTriggerUtil.deleteRelatedEvents( jaList );
        //update Historical Case NOtes
        handler.updateHistoricalCaseNotesOnJobUpdate(trigger.new,trigger.oldMap);
        //update Client Alerts
        handler.updateClientAlertsOnJobUpdate(trigger.new,trigger.oldMap);
        //Create Service Delivery on Job Complete
        handler.createServiceDeliveryOnJobComplete(trigger.new,trigger.oldMap);
        //Update Service Delivery if a completed job is updated finish time
        handler.updateCompletedJobServiceDelivery(trigger.new, trigger.old);

        //create attendee attachments after insert Dispatched Job
        //create attendee attachments after insert Dispatched Job
        skedJsonAvailableServiceUtil.createAttachmentOnAttendeeWhenJobDispatch(trigger.new,trigger.oldMap);
        skedJsonAvailableServiceUtil.createAttachmentOnJobWhenDispatched(trigger.new,trigger.oldMap);
        //Update Geolocation
            skedLocationServices.updateLocationGeocode(Trigger.old, Trigger.newMap, skedLocationServices.OBJ_JOB);
    }

    if(Trigger.isDelete){
        //Explicitly delete related Job Allocations so that related Events are deleted as well.
        list<sked__Job_Allocation__c> jaList = [select id from sked__Job_Allocation__c where sked__Job__c in :trigger.old];
        skedTriggerUtil.deleteRelatedEvents( jaList );
    }
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            //Update the Salesforce1__c field
            handler.jobFieldUpdate(trigger.new);
        }
        if(Trigger.isInsert){
            handler.populdateLocation(trigger.new);
        }
    }
}