trigger skedNoteAttachmentTriggers on enrtcr__Note__c (after insert, after update) {
    
    skedTriggerHandler handler = new skedTriggerHandler();
    
    if(trigger.isInsert || trigger.isUpdate){
    
        //update Related Job's Historical case notes when Note is created
        handler.updateHistoricalCaseNotesOnNotesInsert(trigger.new);

    }


}