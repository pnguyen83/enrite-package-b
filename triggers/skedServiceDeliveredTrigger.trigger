trigger skedServiceDeliveredTrigger on enrtcr__Support_Delivered__c (before insert, after insert) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
        	//RW-44: Map salesforce event id created on job allocation to to field CareRite_EventId_c on the Service Delivered record. 
        	skedTriggerHandler.populateEventID(Trigger.new);
        }
    }
    
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            //RW-88: Update Service Delivery field of Job Service Item
           // skedTriggerHandler.updateJobServiceItemServiceDelivery(trigger.new);
        }
    }
}