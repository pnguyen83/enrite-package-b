trigger skedTimesheetTrigger on Timesheet__c (after insert, after update) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			skedTimesheetTriggerHandler.onAfterInsert(trigger.new);
		}
		else if (trigger.isUpdate) {
			skedTimesheetTriggerHandler.onAfterUpdate(trigger.new);
		}
	}
}